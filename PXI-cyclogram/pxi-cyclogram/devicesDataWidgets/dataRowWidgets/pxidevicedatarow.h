#ifndef PXIDEVICEDATAROW_H
#define PXIDEVICEDATAROW_H

#include <QObject>
#include <QWidget>
#include <QCheckBox>
#include <QDateTimeEdit>
#include <QComboBox>

#include "devicesDataWidgets/dataRowWidgets/controls/pxiclickablelayout.h"
#include "../../dataModel/scheduledatablock.h"
#include "../../common_controls/pxidetailswidget.h"
#include "../../common_controls/pxidurationcontrolswidget.h"
#include "pxischedulesection.h"

class PxiDeviceDataRow : public QObject
{
    Q_OBJECT
public:
    explicit PxiDeviceDataRow(const GlobalDefinitions::ItemType nDeviceType,
                              const int nDeviceIndex,
                              const QString strLabelText,
                              const QPixmap& normalIcon,
                              const QPixmap& selectedIcon,
                              QWidget *parent = nullptr);
    virtual ~PxiDeviceDataRow();
    PxiClickableLayout* deviceLabel() const;
    QFrame* durationWidget() const;
    PxiDetailsWidget* detailsWidget() const;
    void finalizeDetailsWidget();
    int dataRowBlocksCount() const;
    QList<PxiScheduleSection*>& sectionList();
    GlobalDefinitions::ItemType deviceType() const;

    void select();
    void unselect();

    bool selected() const;

    //void recalcRowDuration();
    quint64 rowDuration();

    virtual void fillWithLoadedData(const QJsonArray& );
    virtual bool isEnabledInPeriod( quint64 nStartDuration, quint64 nEndDuration = 0 );

  signals:

    void deviceSelectedSignal(GlobalDefinitions::ItemType,int);
    void dataRowBlocksCountChangedSignal(int blocksCount);
    void showStatusBarMsgSignal(QString&);

public slots:
    virtual void resetDataSlot();
    void addNewDataBlock(GlobalDefinitions::BlockPosition);
    virtual void deleteDataBlockSlot();
    void clearCurrentBlockSlot();
    void durationChangedSlot();

protected:
    QWidget* m_pParent;
    GlobalDefinitions::ItemType m_nDeviceType;
    int m_nDeviceIndex;
    QString m_strDeviceName;
    QList<PxiScheduleSection*> m_lstSections;
    PxiScheduleSection* m_pActiveSection;
    int m_nActiveSectionIndex;
    bool m_bRowSelected;

    int m_setupControlColumn;
    PxiClickableLayout* m_pDeviceSelectableCaption;
    QFrame* m_pDurationWidget;
    PxiDetailsWidget* m_pDetailsWidget;

    QCheckBox* m_pStartTimeCheckBox;
    QDateTimeEdit* m_pStartTimeEdit;
    PxiDurationControlsWidget* m_pDurationControlsWidget;
    QDoubleSpinBox* m_pPollIntervalEdit;

    quint64 m_nRowDuration;
    static quint64 m_nMaxRowDuration;

    static QList<PxiDeviceDataRow*> m_pDataRowsList;

    bool m_bNoSave;

    virtual void setupDurationArea();
    virtual void setupDetailsArea();
    virtual void updateDetailsArea();
    virtual void storeDataControlContents();
    virtual void fillDataControlContents();
    virtual void clearDetailsWidget();

    bool checkForSimultaneousTurnOn(GlobalDefinitions::ItemType deviceCompareTo, bool bActiveSectionOnly = true);

protected slots:
    void labelClickedSlot();
    virtual void switchStateChangedSlot(bool);
    void dataControlValueChangedSlot();

private:
    void updateDurationArea();
    void recalcRowDuration();

  private slots:
    void startResizeSlot();
    void resizingSlot(float percent );
    void stopResizeSlot();

    void sectionSelectedSlot();
};

#endif // PXIDEVICEDATAROW_H

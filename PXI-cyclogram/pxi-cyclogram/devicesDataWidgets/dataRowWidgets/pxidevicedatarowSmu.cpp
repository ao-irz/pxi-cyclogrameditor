#include "pxidevicedatarowSmu.h"
#include "../../global_definitions.h"

PxiDeviceDataRowSmu::PxiDeviceDataRowSmu(const int nDeviceIndex,
                                       const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget *parent):
    PxiDeviceDataRow(GlobalDefinitions::ItemType::ITEM_SMU,
                     nDeviceIndex,
                     strLabelText,
                     normalIcon,
                     selectedIcon,
                     parent)
{
    setupDurationArea();
    setupDetailsArea();
}

double PxiDeviceDataRowSmu::voltageValue() const
{
  return m_pVoltageEdit->value();
}

double PxiDeviceDataRowSmu::voltageTolerancePlusValue() const
{
  return m_pVoltageTolerancePlusEdit->value();
}

double PxiDeviceDataRowSmu::voltageToleranceMinusValue() const
{
  return m_pVoltageToleranceMinusEdit->value();
}

void PxiDeviceDataRowSmu::clearDetailsWidget()
{
  m_bNoSave = true;

  PxiDeviceDataRow::clearDetailsWidget();

  m_pUseVoltageCheck->setChecked(false);

  m_pVoltageEdit->setValue(0);
  m_pVoltageTolerancePlusEdit->setValue(0);
  m_pVoltageToleranceMinusEdit->setValue(0);

  m_pCurrentEdit->setValue(0);
  m_pCurrentTolerancePlusEdit->setValue(0);

  m_pPinNumber->setValue(0);

  m_bNoSave = false;
}

void PxiDeviceDataRowSmu::setupDetailsArea()
{
  // там выставляются контролы времени, единые для всех
      PxiDeviceDataRow::setupDetailsArea();

      QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();

      m_pDurationControlsWidget->setMinimumDuration(2000);
      m_pDurationControlsWidget->setDuration(2000);

  // блок параметров управления напряжением

      m_pUseVoltageCheck = new QCheckBox(m_pDetailsWidget);
      m_pUseVoltageCheck->setProperty( JSON_FIELD_NAME ,JSON_SMU_USE_VOLTAGE);
      m_pUseVoltageCheck->setChecked(false);
      QHBoxLayout* hLayout = new QHBoxLayout();
      hLayout->addWidget(new QLabel("Напряжение",m_pDetailsWidget));
      hLayout->addWidget(m_pUseVoltageCheck);
      connect(m_pUseVoltageCheck, &QCheckBox::stateChanged,
              this,&PxiDeviceDataRowSmu::useVoltageCheckStateChangedSlot);
      pLayout->addLayout(hLayout,0,m_setupControlColumn++);

      m_pPinNumber = new QSpinBox(m_pDetailsWidget);
      m_pPinNumber->setProperty( JSON_FIELD_NAME ,JSON_SMU_PIN_NUMBER);
      m_pPinNumber->setMaximum(7);
      m_pPinNumber->setMinimum(0);
      m_pPinNumber->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pPinNumber,QOverload<int>::of(&QSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      hLayout = new QHBoxLayout();
      QLabel* l = new QLabel("DIO",m_pDetailsWidget);
      hLayout->addWidget(l,0,Qt::AlignRight);
      hLayout->addWidget(m_pPinNumber);
      pLayout->addLayout(hLayout,0,m_setupControlColumn--);

      m_pVoltageEdit = new QDoubleSpinBox(m_pDetailsWidget);
      m_pVoltageEdit->setProperty( JSON_FIELD_NAME ,JSON_SMU_VOLTAGE);
      m_pVoltageEdit->setDecimals(3);
      m_pVoltageEdit->setMaximum(20);
      m_pVoltageEdit->setMinimum(-20);
      m_pVoltageEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pVoltageEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Напряжение (В)",m_pVoltageEdit),
                         1,m_setupControlColumn++);

      m_pVoltageTolerancePlusEdit = new QDoubleSpinBox(m_pDetailsWidget);
      m_pVoltageTolerancePlusEdit->setProperty( JSON_FIELD_NAME ,JSON_SMU_VOLTAGE_TOLERANCE_PLUS);
      m_pVoltageTolerancePlusEdit->setDecimals(3);
      m_pVoltageTolerancePlusEdit->setMaximum(20);
      m_pVoltageTolerancePlusEdit->setMinimum(-20);
      m_pVoltageTolerancePlusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pVoltageTolerancePlusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (+)",m_pVoltageTolerancePlusEdit),
                         1,m_setupControlColumn);

      m_pVoltageToleranceMinusEdit = new QDoubleSpinBox(m_pDetailsWidget);
      m_pVoltageToleranceMinusEdit->setProperty( JSON_FIELD_NAME ,JSON_SMU_VOLTAGE_TOLERANCE_MINUS);
      m_pVoltageToleranceMinusEdit->setDecimals(3);
      m_pVoltageToleranceMinusEdit->setMaximum(20);
      m_pVoltageToleranceMinusEdit->setMinimum(-20);
      m_pVoltageToleranceMinusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pVoltageToleranceMinusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (-)",m_pVoltageToleranceMinusEdit),
                         2,m_setupControlColumn++);


      pLayout->addWidget(m_pDetailsWidget->separatorFrame(),0,m_setupControlColumn++,3,1);
      // блок параметров управления током

      m_pCurrentEdit = new QDoubleSpinBox(m_pDetailsWidget);
      m_pCurrentEdit->setProperty( JSON_FIELD_NAME ,JSON_SMU_CURRENT);
      m_pCurrentEdit->setDecimals(3);
      m_pCurrentEdit->setMaximum(1);
      m_pCurrentEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pCurrentEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Ток (А)",m_pCurrentEdit),
                         1,m_setupControlColumn++);

      m_pCurrentTolerancePlusEdit = new QDoubleSpinBox(m_pDetailsWidget);
      m_pCurrentTolerancePlusEdit->setProperty( JSON_FIELD_NAME ,JSON_SMU_CURRENT_TOLERANCE_PLUS);
      m_pCurrentTolerancePlusEdit->setDecimals(3);
      m_pCurrentTolerancePlusEdit->setMaximum(1);
      m_pCurrentTolerancePlusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
      connect(m_pCurrentTolerancePlusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
              this,&PxiDeviceDataRowSmu::dataControlValueChangedSlot);
      pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (+)",m_pCurrentTolerancePlusEdit),
                         1,m_setupControlColumn);


}

void PxiDeviceDataRowSmu::updateDetailsArea()
{
  PxiDeviceDataRow::updateDetailsArea();

  m_pUseVoltageCheck->setEnabled( m_pActiveSection!=nullptr );

  m_pVoltageEdit->setEnabled( m_pActiveSection!=nullptr && m_pUseVoltageCheck->isChecked() );
  m_pVoltageTolerancePlusEdit->setEnabled( m_pActiveSection!=nullptr && m_pUseVoltageCheck->isChecked() );
  m_pVoltageToleranceMinusEdit->setEnabled( m_pActiveSection!=nullptr && m_pUseVoltageCheck->isChecked() );

  m_pCurrentEdit->setEnabled( m_pActiveSection!=nullptr );
  m_pCurrentTolerancePlusEdit->setEnabled( m_pActiveSection!=nullptr );

  m_pPinNumber->setEnabled( m_pActiveSection!=nullptr );

  if ( m_pActiveSection )
    fillDataControlContents();
}

void PxiDeviceDataRowSmu::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  PxiDeviceDataRow::storeDataControlContents();

  m_pActiveSection->dataBlock()->insert(JSON_SMU_USE_VOLTAGE,QJsonValue(m_pUseVoltageCheck->isChecked()));

  if ( m_pUseVoltageCheck->isChecked() )
  {
    m_pActiveSection->dataBlock()->insert(JSON_SMU_VOLTAGE,QJsonValue(m_pVoltageEdit->value()));
    m_pActiveSection->dataBlock()->insert(JSON_SMU_VOLTAGE_TOLERANCE_PLUS,QJsonValue(m_pVoltageTolerancePlusEdit->value()));
    m_pActiveSection->dataBlock()->insert(JSON_SMU_VOLTAGE_TOLERANCE_MINUS,QJsonValue(m_pVoltageToleranceMinusEdit->value()));
  }
  else
  {
    m_pActiveSection->dataBlock()->remove(JSON_SMU_VOLTAGE);
    m_pActiveSection->dataBlock()->remove(JSON_SMU_VOLTAGE_TOLERANCE_PLUS);
    m_pActiveSection->dataBlock()->remove(JSON_SMU_VOLTAGE_TOLERANCE_MINUS);
  }

  m_pActiveSection->dataBlock()->insert(JSON_SMU_CURRENT,QJsonValue(m_pCurrentEdit->value()));
  m_pActiveSection->dataBlock()->insert(JSON_SMU_CURRENT_TOLERANCE_PLUS,QJsonValue(m_pCurrentTolerancePlusEdit->value()));

  m_pActiveSection->dataBlock()->insert(JSON_SMU_PIN_NUMBER,QJsonValue(m_pPinNumber->value()));
}

void PxiDeviceDataRowSmu::fillDataControlContents()
{
  m_bNoSave = true;

  PxiDeviceDataRow::fillDataControlContents();

  m_pStartTimeCheckBox->setEnabled(false);

  if ( m_pActiveSection )
  {
    m_pUseVoltageCheck->setChecked( m_pActiveSection->dataBlock()->value(JSON_SMU_USE_VOLTAGE).toBool() );

    if ( m_pUseVoltageCheck->isChecked() )
    {
      m_pVoltageEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_VOLTAGE).toDouble());
      m_pVoltageTolerancePlusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_VOLTAGE_TOLERANCE_PLUS).toDouble());
      m_pVoltageToleranceMinusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_VOLTAGE_TOLERANCE_MINUS).toDouble());
    }

    m_pCurrentEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_CURRENT).toDouble());
    m_pCurrentTolerancePlusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_CURRENT_TOLERANCE_PLUS).toDouble());

    m_pPinNumber->setValue(m_pActiveSection->dataBlock()->value(JSON_SMU_PIN_NUMBER).toInt());
  }

  m_bNoSave = false;
}

void PxiDeviceDataRowSmu::switchStateChangedSlot(bool bIsNewStateOn)
{
  if ( !m_pActiveSection )
    return;

  if ( !bIsNewStateOn || !checkForSimultaneousTurnOn(GlobalDefinitions::ItemType::ITEM_DIO) )
    PxiDeviceDataRow::switchStateChangedSlot( bIsNewStateOn );
}

void PxiDeviceDataRowSmu::useVoltageCheckStateChangedSlot(int state)
{
  Qt::CheckState checkState = static_cast<Qt::CheckState>(state);

  m_pVoltageEdit->setValue(0);
  m_pVoltageTolerancePlusEdit->setValue(0);
  m_pVoltageToleranceMinusEdit->setValue(0);

  m_pVoltageEdit->setEnabled( checkState==Qt::CheckState::Checked);
  m_pVoltageTolerancePlusEdit->setEnabled( checkState==Qt::CheckState::Checked);
  m_pVoltageToleranceMinusEdit->setEnabled( checkState==Qt::CheckState::Checked);

  dataControlValueChangedSlot();
}

#ifndef THERMODATAFORM_H
#define THERMODATAFORM_H

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QList>

#include "dataform.h"
#include "../common_controls/pxidetailswidget.h"
#include "../common_controls/pxidurationcontrolswidget.h"
#include "dataRowWidgets/thermoschedulesection.h"
#include "dataRowWidgets/controls/thermodurationbar.h"
#include "dataRowWidgets/thermodurationwidget.h"

class ThermoDataForm : public DataForm
{
    Q_OBJECT
  public:
    explicit ThermoDataForm(QWidget *parent);
    ThermoScheduleSection* section(int idx);

    virtual bool canClose(QString msgTitle = "") override;
    int sectionsCount() const;

  signals:
    void dataRowBlocksCountChangedSignal(int);
    void temperatureChangedSignal(int nSectionIndex);

  public slots:


  protected:
    virtual void setupForm() override;

    virtual bool checkJson( QString& errMsg ) override;
    virtual void distributeData(QString importFileName) override;
    virtual void collectData() override;

  private:

    PxiDetailsWidget* m_pDetailsWidget;
    ThermoDurationWidget* m_pDurationWidget;
    PxiDurationControlsWidget* m_pDurationControlsWidget;

    QComboBox* m_pThermoTypeBox;
    QDoubleSpinBox* m_pTemperatureEdit;
    QDoubleSpinBox* m_pTemperatureChangeRateEdit;
    QCheckBox* m_pTemperatureChangeRateCheck;

    QList<ThermoScheduleSection*> m_lstSections;
    ThermoScheduleSection* m_pActiveSection;
    int m_nActiveSectionIndex;
    bool m_bNoSave;

    void setupDetailsArea();
    void updateDetailsArea();
    void clearDetailsWidget();

    void storeDataControlContents();
    void redrawBars();

  private slots:

    void addNewDataBlock(GlobalDefinitions::BlockPosition);
    void deleteDataBlockSlot();

    void thermoTypeChangedSlot(int idx);
    void switcherValueChangedSlot();

    void startResizeDurationSlot();
    void resizingDurationSlot(float percent);
    void stopResizeDurationSlot();
    void changingTemperatureSlot(int delta);

    void sectionSelectedSlot();
    void durationChangedSlot();
    void temperatureChangedSlot(double);
    void temperatureRateValueChangedSlot(double temperatureChangeRate);
    void temperatureRateCheckStateChangedSlot(int state);
    void clearFormSlot();
};

#endif // THERMODATAFORM_H

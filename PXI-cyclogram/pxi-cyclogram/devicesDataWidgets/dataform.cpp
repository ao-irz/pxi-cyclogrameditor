#include "dataform.h"
#include "../mainwindow.h"

DataForm::DataForm(QWidget *parent) : QWidget(parent)
  ,m_bIsActive(false)
  ,m_strCycloName(CYCLOGRAM_NAME_PREFIX)
  ,m_jsonDataLoaded(QJsonDocument())
  ,m_jsonDataEditing(QJsonDocument())
{
  m_pParent = qobject_cast<PxiCycloMainWindow*>(parent);
}

bool DataForm::canClose(QString msgTitle)
{
  bool retVal = true;

  QMessageBox::StandardButton btnAnsver = QMessageBox::No;

  collectData();

  if ( !m_jsonDataEditing.isEmpty() &&
       m_jsonDataEditing!=m_jsonDataLoaded )
  {
    m_pParent->selectPage(m_nDataFormType);

    QMessageBox* pMessageBox;
    pMessageBox = new QMessageBox(QMessageBox::Question,msgTitle,
                                  "Данные текущей циклограммы не сохранены.\nСохранить данные?",
                                  QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel,m_pParent);
    pMessageBox->setWindowModality(Qt::ApplicationModal);

    btnAnsver = static_cast<QMessageBox::StandardButton>(pMessageBox->exec());

    delete pMessageBox;
  }

  if ( btnAnsver==QMessageBox::Cancel )
    retVal = false;
  if ( btnAnsver==QMessageBox::Yes )
    retVal = saveData();

  return retVal;
}

void DataForm::selectedDataPageSlot(GlobalDefinitions::DataFormPageType nSelectedPage)
{
  m_bIsActive = ( nSelectedPage==m_nDataFormType );
}

void DataForm::showStatusBarMsgSlot(QString& strMsg)
{
  m_pParent->statusBar()->showMessage(strMsg,5000);
}

void DataForm::loadDataSlot()
{
  if ( m_bIsActive )
    loadData();
}

void DataForm::loadData()
{
  QString importFileName = QFileDialog::getOpenFileName(this,"Загрузка данных циклограммы",
                                                        "","Файлы циклограмм (*.json)",
                                                        nullptr,
                                                        QFileDialog::DontUseNativeDialog);
  if ( importFileName=="" )
    return;

  if ( clearForm("Загрузить данные") )
  {
    QFile importFile;
    importFile.setFileName(importFileName);
    if ( importFile.open(QIODevice::ReadOnly|QIODevice::Text) )
    {
      QJsonParseError parseError;
      m_jsonDataLoaded = QJsonDocument::fromJson(importFile.readAll(),&parseError);
      importFile.close();
      if ( m_jsonDataLoaded.isNull() )
      {
        QMessageBox::critical(this,"Загрузить данные",
                              "Ошибка при разборе данных, символ "+QString::number(parseError.offset)+":\n"+
                              parseError.errorString()+"");
        m_jsonDataLoaded = QJsonDocument();
      }
      else
      {
        QString errMsg;
        if ( checkJson( errMsg ) )
        {
          m_jsonDataEditing = m_jsonDataLoaded;
          QFileInfo fInfo(importFileName);
          distributeData(fInfo.fileName());
        }
        else
        {
          QMessageBox::critical(this,"Загрузить данные",
                                errMsg );
          m_jsonDataLoaded = QJsonDocument();
        }
      }
    }
    else
      QMessageBox::critical(this,"Загрузить данные","Не удается открыть файл для чтения");

  }
}

void DataForm::saveDataSlot()
{
  if ( m_bIsActive )
    saveData();
}

bool DataForm::saveData()
{
  switch(m_nDataFormType)
  {
    case GlobalDefinitions::DataFormPageType::THERMO:
    {
      if ( m_pChipsNameEdit->text().length()==0 )
      {
        QMessageBox::critical(this,"Сохранение данных","Не указано наименование термоциклограммы!");
        return false;
      }
      m_strCycloName = m_pChipsNameEdit->text();
      break;
    }
    case GlobalDefinitions::DataFormPageType::PXI:
    {
      if ( m_pChipsNameEdit->text().length()==0 ||
           m_pChipsBatchEdit->text().length()==0 )
      {
        QMessageBox::critical(this,"Сохранение данных","Не указано наименование изделия/номер партии!");
        return false;
      }
      m_strCycloName = m_pChipsNameEdit->text()+"~"+m_pChipsBatchEdit->text();
    }
  }

  QString exportFileName = QFileDialog::getSaveFileName(this,"Сохранение данных циклограммы",
                                                        m_strCycloName+".json","Файлы циклограмм (*.json)",
                                                        nullptr,
                                                        QFileDialog::DontUseNativeDialog);
  if ( exportFileName=="" )
    return false;

  collectData();

  if ( !m_jsonDataEditing.isEmpty() && !m_jsonDataEditing.isNull() )
  {
    QFile exportFile;
    exportFile.setFileName(exportFileName);
    if ( exportFile.open(QIODevice::WriteOnly|QIODevice::Text) )
    {
      exportFile.write(m_jsonDataEditing.toJson());
      exportFile.close();
      QString strMsg = "Данные сохранены в файл "+exportFileName.section("/",-1);
      showStatusBarMsgSlot( strMsg );

      m_jsonDataLoaded = m_jsonDataEditing;

      return true;
    }
    else
    {
      QMessageBox::critical(this,"Сохранение данных","Не удается открыть файл для записи");
      return false;
    }
  }
  else
  {
    QMessageBox::information(this,"Сохранение данных","Нет данных для сохранения!");
    return true;
  }
}

void DataForm::resetDataSlot()
{
  if ( m_bIsActive )
    clearForm("Очистить данные");
}

bool DataForm::clearForm(QString strCaption, bool bForcefully)
{
  bool retVal = false;

  QMessageBox::StandardButton btnAnsver = QMessageBox::Yes;

  if ( !bForcefully )
  {
    collectData();

    if ( !m_jsonDataEditing.isEmpty() &&
         m_jsonDataEditing!=m_jsonDataLoaded )
    {
      QMessageBox* pMessageBox;
      pMessageBox = new QMessageBox(QMessageBox::Question,strCaption,
                                    "Данные текущей циклограммы не сохранены.\nПродолжить загрузку?",QMessageBox::Yes|QMessageBox::No,m_pParent);
      pMessageBox->setWindowModality(Qt::ApplicationModal);

      btnAnsver = static_cast<QMessageBox::StandardButton>(pMessageBox->exec());

      delete pMessageBox;
    }
  }

  if ( btnAnsver==QMessageBox::Yes )
  {
    emit resetDataSignal();

    m_jsonDataLoaded = QJsonDocument();
    m_jsonDataEditing = QJsonDocument();

    m_pChipsNameEdit->setText("");
    m_pChipsBatchEdit->setText("");

    retVal = true;
  }

  return retVal;
}

void DataForm::setupForm()
{
  m_nSetupCurrentRow = 0;

  m_pDataGridLayout = new QGridLayout();
  setLayout(m_pDataGridLayout);
  m_pDataGridLayout->setContentsMargins(5,0,5,5);

  QGridLayout* pNameLayout = new QGridLayout();
  pNameLayout->setSpacing(3);
  m_pChipsNameLabel = new QLabel("Изделие",this);
  pNameLayout->addWidget(m_pChipsNameLabel,0,1);
  m_pChipsBatchLabel = new QLabel("Партия",this);
  pNameLayout->addWidget(m_pChipsBatchLabel,0,2);
  m_pCycloNameLabel = new QLabel(m_strCycloName,this);
  m_pCycloNameLabel->setContentsMargins(0,0,0,0);
  m_pCycloNameLabel->setMargin(0);
  pNameLayout->addWidget(m_pCycloNameLabel,1,0);
  QString nameRegExpPattern = "[^~]+";
  QRegExp regExp = QRegExp(nameRegExpPattern);
  m_pChipsNameEdit = new QLineEdit(this);
  m_pChipsNameEdit->setFixedWidth(100);
  m_pChipsNameEdit->setValidator(new QRegExpValidator(regExp,this));
  m_pChipsBatchEdit = new QLineEdit(this);
  m_pChipsBatchEdit->setFixedWidth(100);
  m_pChipsBatchEdit->setValidator(new QRegExpValidator(regExp,this));
  pNameLayout->addWidget(m_pChipsNameEdit,1,1);
  pNameLayout->addWidget(m_pChipsBatchEdit,1,2);
  pNameLayout->addItem(new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Fixed),1,3);

  m_pDataGridLayout->addLayout(pNameLayout,m_nSetupCurrentRow++,0,1,3);

  m_pHSeparator = new QFrame(this);
  m_pHSeparator->setFrameShape(QFrame::HLine);
  m_pHSeparator->setFrameShadow(QFrame::Sunken);
  m_pDataGridLayout->addWidget(m_pHSeparator,m_nSetupCurrentRow++,0,1,3);
}


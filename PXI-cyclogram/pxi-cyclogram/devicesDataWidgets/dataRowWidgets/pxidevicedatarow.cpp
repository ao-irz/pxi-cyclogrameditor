#include "pxidevicedatarow.h"
#include "pxidevicedatarowGen.h"
#include "pxidevicedatarowTC.h"
#include "pxidevicedatarowSmu.h"
#include "pxidevicedatarowDio.h"
#include <math.h>

quint64 PxiDeviceDataRow::m_nMaxRowDuration = 0;
QList<PxiDeviceDataRow*> PxiDeviceDataRow::m_pDataRowsList;

PxiDeviceDataRow::PxiDeviceDataRow(const GlobalDefinitions::ItemType nDeviceType,
                                   const int nDeviceIndex,
                                   const QString strLabelText,
                                   const QPixmap& normalIcon,
                                   const QPixmap& selectedIcon,
                                   QWidget *parent) : QObject(parent)
  ,m_pParent(parent)
  ,m_nDeviceType(nDeviceType)
  ,m_nDeviceIndex(nDeviceIndex)
  ,m_strDeviceName(strLabelText)
  ,m_pActiveSection(nullptr)
  ,m_nActiveSectionIndex(-1)
  ,m_bRowSelected(false)
  ,m_pDurationWidget(nullptr)
  ,m_pDetailsWidget(nullptr)
  ,m_nRowDuration(0)
  ,m_bNoSave(false)
{
    m_pDeviceSelectableCaption = new PxiClickableLayout(strLabelText, // itemLabel
                                            normalIcon, // normalIcon
                                            selectedIcon,// selectedIcon
                                            parent);
    connect(m_pDeviceSelectableCaption,&PxiClickableLayout::clicked,
            this,&PxiDeviceDataRow::labelClickedSlot);

    m_pDataRowsList.append(this);
}

PxiDeviceDataRow::~PxiDeviceDataRow()
{
  qDeleteAll(m_lstSections);
  m_lstSections.clear();
}

void PxiDeviceDataRow::setupDurationArea()
{
    m_pDurationWidget = new QFrame(m_pParent);
    m_pDurationWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    m_pDurationWidget->setFixedWidth(PXI_DURATION_FRAME_WIDTH);
    m_pDurationWidget->setFrameStyle(QFrame::NoFrame);
    m_pDurationWidget->setStyleSheet("background-color: transparent;");
    m_pDurationWidget->setContentsMargins(0,0,0,0);
}

void PxiDeviceDataRow::setupDetailsArea()
{
    m_pDetailsWidget = new PxiDetailsWidget(m_strDeviceName, m_pParent);

    connect(this,&PxiDeviceDataRow::dataRowBlocksCountChangedSignal,
            m_pDetailsWidget,&PxiDetailsWidget::dataRowBlocksCountChangedSlot);
    connect(m_pDetailsWidget,&PxiDetailsWidget::stateChangedSignal,
            this,&PxiDeviceDataRow::switchStateChangedSlot);
    connect(m_pDetailsWidget,&PxiDetailsWidget::addNewDataBlockBeforeSignal,
            [this](){addNewDataBlock(GlobalDefinitions::BlockPosition::BEFORE);});
    connect(m_pDetailsWidget,&PxiDetailsWidget::deleteDataBlockSignal,
            this,&PxiDeviceDataRow::deleteDataBlockSlot);
    connect(m_pDetailsWidget,&PxiDetailsWidget::addNewDataBlockAfterSignal,
            [this](){addNewDataBlock(GlobalDefinitions::BlockPosition::AFTER);});
    connect(m_pDetailsWidget,&PxiDetailsWidget::clearCurrentBlockSignal,
            this,&PxiDeviceDataRow::clearCurrentBlockSlot);


    QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();
    m_pDetailsWidget->switcher()->setProperty( JSON_FIELD_NAME ,JSON_ENABLE);
    connect(m_pDetailsWidget->switcher(),&PxiSwitcher::valueChanged,
            this,&PxiDeviceDataRow::dataControlValueChangedSlot);

// слева в нулевой колонке остался выключатель
    m_setupControlColumn = 1;
		pLayout->addWidget(m_pDetailsWidget->separatorFrame(),0,m_setupControlColumn++,3,1);

// блок временнЫх переключателей

    m_pStartTimeEdit = new QDateTimeEdit( QDateTime::currentDateTime(), m_pDetailsWidget);
    m_pStartTimeEdit->setProperty( JSON_FIELD_NAME ,JSON_START_TIME);
    m_pStartTimeEdit->setDisplayFormat("dd.MM.yyyy HH:mm:ss");
    m_pStartTimeEdit->setCalendarPopup(true);
    connect(m_pStartTimeEdit,&QDateTimeEdit::dateTimeChanged,
            this,&PxiDeviceDataRow::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createCheckableFieldVLayout("Время запуска",m_pStartTimeEdit,m_pStartTimeCheckBox),
                       0,m_setupControlColumn);
    m_pStartTimeCheckBox->setProperty( JSON_FIELD_NAME ,JSON_START_TIME_ENABLE);
    connect(m_pStartTimeCheckBox,&QCheckBox::stateChanged,
            this,&PxiDeviceDataRow::dataControlValueChangedSlot);

    m_pDurationControlsWidget = new PxiDurationControlsWidget(m_pDetailsWidget);
    m_pDurationControlsWidget->setProperty( JSON_FIELD_NAME ,JSON_DURATION);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Продолжительность (ч:м:с)",m_pDurationControlsWidget),
                       1,m_setupControlColumn);
    connect(m_pDurationControlsWidget,&PxiDurationControlsWidget::durationChangedSignal,
            this,&PxiDeviceDataRow::durationChangedSlot);
    connect(m_pDurationControlsWidget,&PxiDurationControlsWidget::valueChanged,
            this,&PxiDeviceDataRow::dataControlValueChangedSlot);

    m_pPollIntervalEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pPollIntervalEdit->setProperty( JSON_FIELD_NAME ,JSON_POLL_INTERVAL);
    m_pPollIntervalEdit->setMaximum(999.999);
    m_pPollIntervalEdit->setDecimals(3);
    connect(m_pPollIntervalEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRow::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Интервал опроса (с)",m_pPollIntervalEdit),
                       2,m_setupControlColumn++);

		pLayout->addWidget(m_pDetailsWidget->separatorFrame(),0,m_setupControlColumn++,3,1);
}

void PxiDeviceDataRow::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  if ( m_pStartTimeCheckBox->isChecked() )
  {
    m_pActiveSection->dataBlock()->insert(JSON_START_TIME,QJsonValue(m_pStartTimeEdit->dateTime().toString("yyyy-MM-dd HH:mm:ss")));
  }
  else
    m_pActiveSection->dataBlock()->remove(JSON_START_TIME);

  m_pActiveSection->dataBlock()->insert(JSON_ENABLE,QJsonValue(m_pDetailsWidget->switcher()->valueBool()));
  m_pActiveSection->dataBlock()->insert(JSON_DURATION,QJsonValue(m_pDurationControlsWidget->durationStr()));
  m_pActiveSection->dataBlock()->insert(JSON_POLL_INTERVAL,QJsonValue(round(m_pPollIntervalEdit->value()*1000)));
}

void PxiDeviceDataRow::fillDataControlContents()
{
  if ( m_pActiveSection )
  {
    ScheduleDataBlock* jsonDataBlock = m_pActiveSection->dataBlock();
    m_pDetailsWidget->switcher()->setState(jsonDataBlock->value(JSON_ENABLE).toBool());

    m_pStartTimeCheckBox->setEnabled(m_nActiveSectionIndex==0);
    if ( jsonDataBlock->value(JSON_START_TIME) == QJsonValue::Undefined )
    {
      m_pStartTimeCheckBox->setChecked(false);
      m_pStartTimeEdit->setEnabled(false);
      m_pStartTimeEdit->setDateTime(QDateTime::currentDateTime());
    }
    else
    {
      m_pStartTimeCheckBox->setChecked(true);
      m_pStartTimeEdit->setEnabled(true);
      m_pStartTimeEdit->setDateTime(QDateTime::fromString(jsonDataBlock->value(JSON_START_TIME).toString(),"yyyy-MM-dd HH:mm:ss"));
    }

    m_pDurationControlsWidget->setDuration(jsonDataBlock->value(JSON_DURATION).toString());

    m_pPollIntervalEdit->setValue(jsonDataBlock->value(JSON_POLL_INTERVAL).toDouble()/1000);
  }
}

void PxiDeviceDataRow::updateDetailsArea()
{
  if ( m_pActiveSection )
  {
    m_pDetailsWidget->switcher()->setState(m_pActiveSection->dataBlock()->isEnabled());

    m_pDurationControlsWidget->setEnabled(true);
    m_pDurationControlsWidget->setDuration(m_pActiveSection->dataBlock()->durationInt());

    m_pPollIntervalEdit->setEnabled(true);
  }
  else
  {
    m_pDetailsWidget->switcher()->setEnabled(false);
    m_pStartTimeCheckBox->setEnabled(false);
    m_pStartTimeCheckBox->setChecked(false);
    m_pDurationControlsWidget->setEnabled(false);
    m_pPollIntervalEdit->setEnabled(false);
  }
}


void PxiDeviceDataRow::fillWithLoadedData(const QJsonArray& dataBlocksArray)
{
  for(auto dataBlocksArrayElement : dataBlocksArray)
  {
    addNewDataBlock(GlobalDefinitions::BlockPosition::AFTER);

    QJsonObject dataBlock = dataBlocksArrayElement.toObject();
    for( auto dataKey: dataBlock.keys() )
    {
      m_pActiveSection->dataBlock()->insert(dataKey,dataBlock.value(dataKey));
      if ( dataKey==JSON_DURATION )
        m_pActiveSection->dataBlock()->setDuration(dataBlock.value(dataKey).toString());
      if ( dataKey==JSON_ENABLE )
        switchStateChangedSlot(dataBlock.value(dataKey).toBool());
    }
    if ( !dataBlock.keys().contains(JSON_IMPEDANCE) && dataBlock.value(JSON_ENABLE).toBool()  )
      m_pActiveSection->dataBlock()->remove(JSON_IMPEDANCE);

    fillDataControlContents();
  }

  recalcRowDuration();
}

bool PxiDeviceDataRow::isEnabledInPeriod(  quint64 nPeriodDurationBegin,
                                           quint64 nPeriodDurationEnd )
{
  bool retVal = false;

  quint64 nSectionDurationBegin = 0;
  quint64 nSectionDurationEnd;

  if ( nPeriodDurationEnd==0 )
  {
    for( auto pSection : m_lstSections )
    {
      nSectionDurationEnd = nSectionDurationBegin+pSection->duration();
      if ( nPeriodDurationBegin>=nSectionDurationBegin &&
           nPeriodDurationBegin<nSectionDurationEnd )
      {
        retVal = pSection->dataBlock()->isEnabled();
        break;
      }
      else
        nSectionDurationBegin += pSection->duration();
    }
  }
  else
  if ( nPeriodDurationEnd>nPeriodDurationBegin )
  {
    for( auto pSection : m_lstSections )
    {
      nSectionDurationEnd = nSectionDurationBegin+pSection->duration();
      if ( !pSection->dataBlock()->isEnabled() )
        nSectionDurationBegin += pSection->duration();
      else
      {
        if ( ( nSectionDurationEnd>=nPeriodDurationBegin && nSectionDurationEnd<nPeriodDurationEnd ) ||
             ( nSectionDurationBegin>=nPeriodDurationBegin && nSectionDurationBegin<nPeriodDurationEnd ) ||
             ( nSectionDurationBegin<=nPeriodDurationBegin && nSectionDurationEnd>nPeriodDurationEnd )
           )
        {
          retVal = true;
          break;
        }
      }
    }
  }

  return retVal;
}

void PxiDeviceDataRow::deleteDataBlockSlot()
{

  if ( m_pActiveSection )
  {
    m_lstSections.removeAt(m_nActiveSectionIndex);
    delete m_pActiveSection;

    if ( m_nActiveSectionIndex>=m_lstSections.count() )
      m_nActiveSectionIndex--;

    if ( m_nActiveSectionIndex>=0 )
    {
      m_pActiveSection = m_lstSections[m_nActiveSectionIndex];
      m_pActiveSection->select();

      recalcRowDuration();
    }
    else
    {
      m_pActiveSection = nullptr;
      clearDetailsWidget();
    }
  }

  updateDetailsArea();
  emit dataRowBlocksCountChangedSignal(dataRowBlocksCount());
}

void PxiDeviceDataRow::addNewDataBlock(GlobalDefinitions::BlockPosition nBlockPosition)
{
  clearDetailsWidget();

  switch(nBlockPosition)
  {
    case GlobalDefinitions::BlockPosition::BEFORE:
    {
      if (m_nActiveSectionIndex==0)
        m_pActiveSection->dataBlock()->remove(JSON_START_TIME);

      m_pActiveSection = new PxiScheduleSection(this,m_pDurationWidget);
      if (m_nActiveSectionIndex<0)
        m_nActiveSectionIndex++;
      break;
    }
    case GlobalDefinitions::BlockPosition::AFTER:
    {
      m_pActiveSection = new PxiScheduleSection(this,m_pDurationWidget);
      m_nActiveSectionIndex++;
      break;
    }
  }
  m_pActiveSection->addDataBlock(new ScheduleDataBlock());

  PxiDurationBar* newBar = new PxiDurationBar(m_pDurationWidget,m_pActiveSection);
  newBar->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);
  newBar->setFixedHeight(20);

  newBar->resizeBar(1);
  newBar->show();

  connect(newBar,&PxiDurationBar::startResizeSignal,this,&PxiDeviceDataRow::startResizeSlot);
  connect(newBar,&PxiDurationBar::resizingSignal,this,&PxiDeviceDataRow::resizingSlot);
  connect(newBar,&PxiDurationBar::stopResizeSignal,this,&PxiDeviceDataRow::stopResizeSlot);

  m_pActiveSection->addBar(newBar);
  m_lstSections.insert(m_nActiveSectionIndex,m_pActiveSection);
  connect(m_pActiveSection,&PxiScheduleSection::selectedSignal,
          this,&PxiDeviceDataRow::sectionSelectedSlot);

  recalcRowDuration();

  emit dataRowBlocksCountChangedSignal(dataRowBlocksCount());

  m_pActiveSection->select();

}

void PxiDeviceDataRow::clearCurrentBlockSlot()
{
  if ( m_pActiveSection )
  {
    clearDetailsWidget();
    m_pDetailsWidget->switcher()->setState(false);
    switchStateChangedSlot(false);
    m_pDurationControlsWidget->setDuration(24*60*60*1000);
    m_pActiveSection->dataBlock()->setDuration(24*60*60*1000);
    recalcRowDuration();
    storeDataControlContents();
  }
}

void PxiDeviceDataRow::clearDetailsWidget()
{
  m_pStartTimeCheckBox->setChecked(false);
  m_pStartTimeEdit->setDateTime(QDateTime::currentDateTime());

  if ( m_pActiveSection )
    m_pDurationControlsWidget->setDuration(m_pActiveSection->dataBlock()->durationInt());
  else
  {
    switchStateChangedSlot(false);
    m_pDurationControlsWidget->clear();

    m_pDetailsWidget->switcher()->setState(false);
  }

  m_pPollIntervalEdit->setValue(0);
}

PxiClickableLayout* PxiDeviceDataRow::deviceLabel() const
{
  return m_pDeviceSelectableCaption;
}

QFrame* PxiDeviceDataRow::durationWidget() const
{
    return m_pDurationWidget;
}

PxiDetailsWidget* PxiDeviceDataRow::detailsWidget() const
{
    return m_pDetailsWidget;
}

void PxiDeviceDataRow::select()
{
    m_pDeviceSelectableCaption->select();
    m_pDurationWidget->setStyleSheet("background-color: white;");
    m_bRowSelected = true;

    if ( m_pActiveSection )
      m_pActiveSection->select();

    updateDetailsArea();
}

void PxiDeviceDataRow::unselect()
{
    m_pDeviceSelectableCaption->unselect();
    m_pDurationWidget->setStyleSheet("background-color: transparent;");
    m_bRowSelected = false;

    for (auto& section : m_lstSections)
        section->unselect();
}

bool PxiDeviceDataRow::selected() const
{
  return m_bRowSelected;
}

void PxiDeviceDataRow::recalcRowDuration()
{
  m_nRowDuration = 0;
  for(auto& section : m_lstSections )
    m_nRowDuration += section->dataBlock()->durationInt();

  m_nMaxRowDuration = 0;
  for( auto dataRow : m_pDataRowsList )
  {
    if ( m_nMaxRowDuration < dataRow->rowDuration() )
      m_nMaxRowDuration = dataRow->rowDuration();
  }
  for( auto dataRow : m_pDataRowsList )
    dataRow->updateDurationArea();

  if ( m_nDeviceType==GlobalDefinitions::ItemType::ITEM_DIO )
    checkForSimultaneousTurnOn(GlobalDefinitions::ItemType::ITEM_SMU,false);
  if ( m_nDeviceType==GlobalDefinitions::ItemType::ITEM_SMU )
    checkForSimultaneousTurnOn(GlobalDefinitions::ItemType::ITEM_DIO,false);
}

void PxiDeviceDataRow::updateDurationArea()
{
  int rowBarsTotalWidth = static_cast<int>((PXI_DURATION_FRAME_WIDTH-(DURATION_BLOCKS_SPACING*2))*static_cast<double>(m_nRowDuration)/m_nMaxRowDuration-
                                           DURATION_BLOCKS_SPACING*(m_lstSections.count()-1));

  int barLeftPoint = DURATION_BLOCKS_SPACING;
  for(auto& section : m_lstSections )
  {
    int barWidth = static_cast<int>(rowBarsTotalWidth*static_cast<double>(section->dataBlock()->durationInt())/m_nRowDuration);
    if ( barWidth<=DURATION_BLOCKS_SPACING )
      barWidth = DURATION_BLOCKS_SPACING;
    section->bar()->move(barLeftPoint,DURATION_WIDGET_TOP_MARGIN);
    section->bar()->resizeBar(barWidth);
    barLeftPoint += barWidth+DURATION_BLOCKS_SPACING;
  }
}

quint64 PxiDeviceDataRow::rowDuration()
{
  return m_nRowDuration;
}

void PxiDeviceDataRow::labelClickedSlot()
{
  emit deviceSelectedSignal(m_nDeviceType,m_nDeviceIndex);
  select();
}

void PxiDeviceDataRow::switchStateChangedSlot(bool bNewState)
{
  if ( !m_pActiveSection )
    return;

  m_pActiveSection->dataBlock()->setEnable( bNewState );
  m_pActiveSection->bar()->setOnState(bNewState);
}

void PxiDeviceDataRow::startResizeSlot()
{
  if ( m_pActiveSection )
    m_pActiveSection->dataBlock()->setResizing(true);
}

void PxiDeviceDataRow::resizingSlot( float percent )
{
  m_pActiveSection->dataBlock()->recalcDuration( percent );
  updateDetailsArea();
  recalcRowDuration();
}


void PxiDeviceDataRow::stopResizeSlot()
{
  if ( m_pActiveSection )
    m_pActiveSection->dataBlock()->setResizing(false);
}

void PxiDeviceDataRow::durationChangedSlot()
{
  if ( m_pActiveSection )
  {
    m_pActiveSection->dataBlock()->setDuration(m_pDurationControlsWidget->durationInt());
    recalcRowDuration();
  }
}

void PxiDeviceDataRow::sectionSelectedSlot()
{
  PxiScheduleSection* senderSection = qobject_cast<PxiScheduleSection*>(sender());
  m_pActiveSection = senderSection;

  for (int idx=0; idx<m_lstSections.count(); idx++)
  {
    if ( m_lstSections[idx]!=m_pActiveSection )
      m_lstSections[idx]->unselect();
    else
      m_nActiveSectionIndex = idx;
  }

  updateDetailsArea();

  if ( !m_bRowSelected )
    labelClickedSlot();
}

void PxiDeviceDataRow::resetDataSlot()
{
  while (dataRowBlocksCount()>0)
    deleteDataBlockSlot();
}

int PxiDeviceDataRow::dataRowBlocksCount() const
{
  return m_lstSections.count();
}

QList<PxiScheduleSection *>& PxiDeviceDataRow::sectionList()
{
  return m_lstSections;
}

GlobalDefinitions::ItemType PxiDeviceDataRow::deviceType() const
{
  return m_nDeviceType;
}

void PxiDeviceDataRow::finalizeDetailsWidget()
{
    int i;

    m_pDetailsWidget->detailsLayout()->addItem(new QSpacerItem(1,1,QSizePolicy::Fixed,QSizePolicy::MinimumExpanding),
                                        m_pDetailsWidget->detailsLayout()->rowCount(),0);
    for (i=0; i<m_pDetailsWidget->detailsLayout()->rowCount()-1;i++)
      m_pDetailsWidget->detailsLayout()->setRowStretch(i,0);
    m_pDetailsWidget->detailsLayout()->setRowStretch(i,1);

    m_pDetailsWidget->detailsLayout()->addItem(new QSpacerItem(1,1,QSizePolicy::MinimumExpanding,QSizePolicy::Fixed),
                                        0,m_pDetailsWidget->detailsLayout()->columnCount());
    for (i=0; i<m_pDetailsWidget->detailsLayout()->columnCount()-1;i++)
      m_pDetailsWidget->detailsLayout()->setColumnStretch(i,0);
    m_pDetailsWidget->detailsLayout()->setColumnStretch(i,1);

    emit dataRowBlocksCountChangedSignal(dataRowBlocksCount());
}

bool PxiDeviceDataRow::checkForSimultaneousTurnOn(GlobalDefinitions::ItemType deviceCompareTo, bool bActiveSectionOnly )
{
  bool bError = false;

  quint64 nSectionDurationBegin=0;
  quint64 nSectionDurationEnd=0;

  for( const auto pSection : qAsConst(m_lstSections) )
  {
    nSectionDurationEnd = nSectionDurationBegin + pSection->duration();

    if ( (!bActiveSectionOnly && pSection->dataBlock()->isEnabled()) ||
         ( bActiveSectionOnly && pSection==m_pActiveSection )
       )
    {
      for( const auto pDataRow : qAsConst(m_pDataRowsList) )
      {
        if ( pDataRow->deviceType()==deviceCompareTo )
        {
          if ( pDataRow->isEnabledInPeriod( nSectionDurationBegin, nSectionDurationEnd) )
          {
            if ( bActiveSectionOnly && pSection==m_pActiveSection )
              m_pDetailsWidget->switcher()->setState(false);

            QString strMsg = DIO_SMU_SIMULTANEOUS_ERROR_MSG;
            emit showStatusBarMsgSignal(strMsg);
            bError = true;
            break;
          }
        }
      }
    }

    if ( bError )
      break;

    nSectionDurationBegin += pSection->duration();
  }


  return bError;
}

void PxiDeviceDataRow::dataControlValueChangedSlot()
{
  QString jsonField = "";
  QWidget* senderWidget = qobject_cast<QWidget*>(sender());
  if ( senderWidget && senderWidget->property( JSON_FIELD_NAME ).isValid() )
    jsonField = senderWidget->property( JSON_FIELD_NAME ).toString();

  if ( m_bNoSave || jsonField=="" || !m_pActiveSection )
    return;

  PxiSwitcher* switcher = qobject_cast<PxiSwitcher*>(sender());
  QDateTimeEdit* dateTimeEdit = qobject_cast<QDateTimeEdit*>(sender());
  PxiDurationControlsWidget* durationControl = qobject_cast<PxiDurationControlsWidget*>(sender());
  QComboBox* comboBox = qobject_cast<QComboBox*>(sender());
  QDoubleSpinBox* doubleSpinBox = qobject_cast<QDoubleSpinBox*>(sender());
  QSpinBox* spinBox = qobject_cast<QSpinBox*>(sender());
  QCheckBox* checkBox = qobject_cast<QCheckBox*>(sender());
  PxiDioPin* pinWidget = qobject_cast<PxiDioPin*>(sender());

  ScheduleDataBlock* jsonDataBlock = m_pActiveSection->dataBlock();

  if ( switcher )
    jsonDataBlock->insert(jsonField,QJsonValue(switcher->valueBool()));

  if ( checkBox )
  {
    if ( jsonField==JSON_START_TIME_ENABLE )
    {
      if ( checkBox->isChecked() )
        jsonDataBlock->insert(JSON_START_TIME,QJsonValue(m_pStartTimeEdit->dateTime().toString("yyyy-MM-dd HH:mm:ss")));
      else
        jsonDataBlock->remove(JSON_START_TIME);
    }
    else
    if ( jsonField==JSON_IMPEDANCE )
    {
      if ( checkBox->isChecked() )
        jsonDataBlock->insert(JSON_IMPEDANCE,QJsonValue(50));
      else
        jsonDataBlock->remove(JSON_IMPEDANCE);

      jsonDataBlock->insert(JSON_AMPLITUDE, QJsonValue(static_cast<PxiDeviceDataRowGen*>(this)->amplitudeValue()));
      jsonDataBlock->insert(JSON_OFFSET, QJsonValue(static_cast<PxiDeviceDataRowGen*>(this)->offsetValue()));
    }
    else
    if ( jsonField==JSON_TEMPERATURE_MAX_ENABLED )
    {
      if ( checkBox->isChecked() )
        jsonDataBlock->insert(JSON_TEMPERATURE_MAX, QJsonValue(static_cast<PxiDeviceDataRowThermoCouple*>(this)->temperatureMaxValue()));
      else
        jsonDataBlock->remove(JSON_TEMPERATURE_MAX);
    }
    else
    if ( jsonField==JSON_TEMPERATURE_MIN_ENABLED )
    {
      if ( checkBox->isChecked() )
        jsonDataBlock->insert(JSON_TEMPERATURE_MIN, QJsonValue(static_cast<PxiDeviceDataRowThermoCouple*>(this)->temperatureMinValue()));
      else
        jsonDataBlock->remove(JSON_TEMPERATURE_MIN);

    }
    else
    if ( jsonField==JSON_SMU_USE_VOLTAGE )
    {
      jsonDataBlock->insert(JSON_SMU_USE_VOLTAGE,QJsonValue(checkBox->isChecked()));
      if ( checkBox->isChecked() )
      {
        jsonDataBlock->insert(JSON_SMU_VOLTAGE,QJsonValue(static_cast<PxiDeviceDataRowSmu*>(this)->voltageValue()));
        jsonDataBlock->insert(JSON_SMU_VOLTAGE_TOLERANCE_PLUS,QJsonValue(static_cast<PxiDeviceDataRowSmu*>(this)->voltageTolerancePlusValue()));
        jsonDataBlock->insert(JSON_SMU_VOLTAGE_TOLERANCE_MINUS,QJsonValue(static_cast<PxiDeviceDataRowSmu*>(this)->voltageToleranceMinusValue()));
      }
      else
      {
        jsonDataBlock->remove(JSON_SMU_VOLTAGE);
        jsonDataBlock->remove(JSON_SMU_VOLTAGE_TOLERANCE_PLUS);
        jsonDataBlock->remove(JSON_SMU_VOLTAGE_TOLERANCE_MINUS);
      }
    }
  }

  if ( dateTimeEdit && m_pStartTimeCheckBox->isEnabled() && m_pStartTimeCheckBox->isChecked() )
    jsonDataBlock->insert(JSON_START_TIME,QJsonValue(m_pStartTimeEdit->dateTime().toString("yyyy-MM-dd HH:mm:ss")));

  if ( durationControl )
    jsonDataBlock->insert(JSON_DURATION,QJsonValue(durationControl->durationStr()));

  if ( doubleSpinBox )
  {
    if ( jsonField==JSON_POLL_INTERVAL )
      jsonDataBlock->insert(JSON_POLL_INTERVAL,QJsonValue(round(doubleSpinBox->value()*1000)));
    else
    if ( jsonField==JSON_FREQUENCY )
      jsonDataBlock->insert(JSON_FREQUENCY, QJsonValue(static_cast<PxiDeviceDataRowGen*>(this)->frequency()));
    else
      jsonDataBlock->insert(jsonField,QJsonValue(QString::number(doubleSpinBox->value(),'f',3).toDouble()));
  }

  if ( comboBox )
  {
    if ( jsonField==JSON_FREQUENCY )
      jsonDataBlock->insert(JSON_FREQUENCY, QJsonValue(static_cast<PxiDeviceDataRowGen*>(this)->frequency()));
    else
    if ( jsonField==JSON_FUNCTION_TYPE )
    {
      GlobalDefinitions::GenFunction function = static_cast<PxiDeviceDataRowGen*>(this)->functionType();
      jsonDataBlock->insert(JSON_FUNCTION_TYPE, QJsonValue(QVariant::fromValue(function).toString().toLower()));
      if ( function==GlobalDefinitions::GenFunction::SINUS ||
           function==GlobalDefinitions::GenFunction::TRIANGLE )
        jsonDataBlock->remove(JSON_DUTYCYCLE);
      else
        jsonDataBlock->insert(JSON_DUTYCYCLE,QJsonValue(static_cast<PxiDeviceDataRowGen*>(this)->dutyCycle()));
    }
    else
    if ( jsonField==JSON_DIO_VOLTAGE_LOGIC )
      jsonDataBlock->insert(JSON_DIO_VOLTAGE_LOGIC,
                            QJsonValue(static_cast<PxiDeviceDataRowDio*>(this)->voltageLogic()));
  }

  if ( spinBox )
    jsonDataBlock->insert(jsonField,QJsonValue(spinBox->value()));

  if ( pinWidget )
  {
    static_cast<PxiDeviceDataRowDio*>(this)->storePinsArray();
  }
}

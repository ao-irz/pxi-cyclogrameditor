#ifndef PXIDEVICEDATAROWGEN_H
#define PXIDEVICEDATAROWGEN_H

#include <QObject>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QJsonArray>

#include "pxidevicedatarow.h"

class PxiDeviceDataRowGen : public PxiDeviceDataRow
{
    Q_OBJECT

public:
    PxiDeviceDataRowGen(const int nDeviceIndex,
                       const QString strLabelText,
                       const QPixmap& normalIcon,
                       const QPixmap& selectedIcon,
                       QWidget *parent = nullptr);
    double amplitudeValue() const;
    double offsetValue() const;
    double frequency() const;
    GlobalDefinitions::GenFunction functionType() const;
    int dutyCycle() const;

  public slots:
    virtual void clearDetailsWidget() override;

protected:
    virtual void setupDetailsArea() override;
    virtual void updateDetailsArea() override;
    virtual void storeDataControlContents() override;
    virtual void fillDataControlContents() override;

private:

    QComboBox* m_pFunctionBox;
    QWidget* m_pDutyCycleWidget;
    QSpinBox* m_pDutyCycleEdit;
    QDoubleSpinBox* m_pFrequencyEdit;
    QComboBox* m_pFrequencyDimensionBox;
    uint m_nMaxFrequency;
    uint m_nFrequencyMultiplier;
    QLabel* m_pFrequencyLabel;
    QDoubleSpinBox* m_pAmplitudeEdit;
    QDoubleSpinBox* m_pOffsetEdit;
    QCheckBox* m_pImpedanceCheck;
    int nMaxAmplitude;
    double m_nFrequency;
    GlobalDefinitions::GenFunction m_enCurrentFunction;

    QVBoxLayout* createFrequencyLayout();

  private slots:
    void functionChangedSlot();
    void frequencyChangedSlot();
    void impedanceStateChangedSlot(int state);
    void amplitudeValueChangedSlot(double newValue);
    void offsetValueChangedSlot(double newValue);
};

#endif // PXIDEVICEDATAROWGEN_H

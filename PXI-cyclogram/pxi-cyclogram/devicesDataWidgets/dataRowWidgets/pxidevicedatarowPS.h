#ifndef PXIDEVICEDATAROWPS_H
#define PXIDEVICEDATAROWPS_H

#include <QObject>

#include "pxidevicedatarow.h"

class PxiDeviceDataRowPS : public PxiDeviceDataRow
{
    Q_OBJECT

public:
    PxiDeviceDataRowPS(const int nDeviceIndex,
                       const QString strLabelText,
                       const QPixmap& normalIcon,
                       const QPixmap& selectedIcon,
                       QWidget *parent = nullptr);

public slots:
    virtual void clearDetailsWidget() override;

protected:
    virtual void setupDetailsArea() override;
    virtual void updateDetailsArea() override;
    virtual void storeDataControlContents() override;
    virtual void fillDataControlContents() override;

private:
    QDoubleSpinBox* m_pVoltageEdit;
    QDoubleSpinBox* m_pVoltageTolerancePlusEdit;
    QDoubleSpinBox* m_pVoltageToleranceMinusEdit;

    QDoubleSpinBox* m_pCurrentEdit;
    QDoubleSpinBox* m_pCurrentTolerancePlusEdit;
    QDoubleSpinBox* m_pCurrentToleranceMinusEdit;

private slots:
    //void setVoltageToleranceLimitsSlot(double);

};

#endif // PXIDEVICEDATAROWPS_H

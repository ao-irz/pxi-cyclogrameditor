#ifndef GLOBAL_DEFINITIONS_H
#define GLOBAL_DEFINITIONS_H

#include <QObject>

//#define SLOT_NUMBER  1
//#define SLOT_NAME   "Слот"

#define POWER_SUPPLY_NUMBER 4
#define OVEN_MAX_TEMPERATURE 150
#define OVEN_MIN_TEMPERATURE -60
#define OVEN_DEFAULT_TEMPERATURE 25

#define POWER_SUPPLY_NAME "ИП"
#define GENERATOR_NAME "Генератор"
#define THERMOCOUPLE_NAME "Термопара"
#define DIO_NAME "DIO"
#define SUO_NAME "SMU"

#define LOAD_TOOLTIP "Загрузить циклограмму из файла"
#define SAVE_TOOLTIP "Сохранить циклограмму в файл"
#define CLEAR_TOOLTIP "Очистить всю циклограмму"
#define CLEAR_BLOCK_TOOLTIP "Очистить текущий блок"
#define DELETE_BLOCK_TOOLTIP "Удалить текущий блок"
#define ADD_BLOCK_BEFORE_TOOLTIP "Добавить блок перед"
#define ADD_BLOCK_AFTER_TOOLTIP "Добавить блок после"
#define CYCLOGRAM_NAME_PREFIX "Циклограмма: "
#define DIO_SMU_SIMULTANEOUS_ERROR_MSG "Устройства DIO и SMU не могут быть включены одновременно!"
#define THERMO_COOLING_TOOLTIP "Охлаждение"
#define THERMO_MAINTAINING_TOOLTIP "Поддержание"
#define THERMO_HEATING_TOOLTIP "Нагрев"

#define POWER_SUPPLY_ICON ":powersupply.png"
#define GENERATOR_ICON ":generator.png"
#define THERMOCOUPLE_ICON ":thermocouple.png"
#define DIO_ICON ":dio.png"
#define POWER_SUPPLY_SELECTED_ICON ":powersupply_selected.png"
#define GENERATOR_SELECTED_ICON ":generator_selected.png"
#define THERMOCOUPLE_SELECTED_ICON ":thermocouple_selected.png"
#define DIO_SELECTED_ICON ":dio_selected.png"
#define LOAD_ICON ":load.png"
#define SAVE_ICON ":save.png"
#define CLEAR_ICON ":clear.png"
#define ADD_AFTER_ICON ":addAfter.png"
#define ADD_BEFORE_ICON ":addBefore.png"
#define MINUS_ICON ":minus.png"
#define HEATING_ICON ":heating.png"
#define MAINTAINING_ICON ":maintaining.png"
#define COOLING_ICON ":cooling.png"

#define DEVICES_CYCLOGRAM ":devices_cyclogram.png"
#define THERMO_CYCLOGRAM ":thermo_cyclogram.png"

#define ITEM_TYPE_KEY   "itemTypeKey"
#define SLOT_ID_KEY   "slotIdKey"
#define ITEM_ID_KEY   "itemIdKey"

#define PXI_DURATION_FRAME_WIDTH 1140
#define DURATION_BLOCKS_SPACING 3
#define DURATION_WIDGET_TOP_MARGIN 6
#define DURATION_BLOCKS_RESIZE_STEP 3
#define THERMO_FRAME_WIDTH 1290
#define THERMO_FRAME_HEIGHT 298
#define THERMO_FRAME_GRID_WIDTH 1250
#define THERMO_FRAME_GRID_HEIGHT 290
#define THERMO_FRAME_GRID_LEFT_MARGIN 30
#define THERMO_FRAME_GRID_TOP_MARGIN 10
#define THERMO_FRAME_GRID_LINE_STEP 13
#define THERMO_DURATION_BLOCK_DEFAULT_WIDTH 313
#define THERMO_DURATION_BLOCK_MINIMUM_WIDTH 10
#define THERMO_DURATION_BLOCK_DEFAULT_HEIGHT THERMO_FRAME_GRID_HEIGHT-THERMO_FRAME_GRID_TOP_MARGIN
#define THERMO_DURATION_BLOCK_SPACING 1
#define THERMO_DURATION_BLOCKS_TOTAL_WIDTH THERMO_FRAME_GRID_WIDTH-2

#define JSON_FIELD_NAME "jsonField"
#define JSON_ENABLE "enable"
#define JSON_START_TIME_ENABLE "startTimeEnabled"
#define JSON_START_TIME "startTime"
#define JSON_DURATION "duration"
#define JSON_POLL_INTERVAL "pollInterval"
#define JSON_VOLTAGE "voltage"
#define JSON_VOLTAGE_TOLERANCE_PLUS "voltageTolerancePlus"
#define JSON_VOLTAGE_TOLERANCE_MINUS "voltageToleranceMinus"
#define JSON_CURRENT "current"
#define JSON_CURRENT_TOLERANCE_PLUS "currentTolerancePlus"
#define JSON_CURRENT_TOLERANCE_MINUS "currentToleranceMinus"
#define JSON_SMU_PIN_NUMBER "pinNumber"
#define JSON_SMU_USE_VOLTAGE "useVoltage"
#define JSON_SMU_VOLTAGE "voltage"
#define JSON_SMU_VOLTAGE_TOLERANCE_PLUS "voltageTolerancePlus"
#define JSON_SMU_VOLTAGE_TOLERANCE_MINUS "voltageToleranceMinus"
#define JSON_SMU_CURRENT "current"
#define JSON_SMU_CURRENT_TOLERANCE_PLUS "currentTolerancePlus"
#define JSON_SMU_CURRENT_TOLERANCE_MINUS "currentToleranceMinus"
#define JSON_FUNCTION_TYPE "type"
#define JSON_DUTYCYCLE "dutycycle"
#define JSON_FREQUENCY "frequency"
#define JSON_AMPLITUDE "amplitude"
#define JSON_OFFSET "offset"
#define JSON_IMPEDANCE "impedance"
#define JSON_TEMPERATURE_MAX "temperatureMax"
#define JSON_TEMPERATURE_MIN "temperatureMin"
#define JSON_TEMPERATURE_MAX_ENABLED "temperatureMaxEnabled"
#define JSON_TEMPERATURE_MIN_ENABLED "temperatureMinEnabled"
#define JSON_DIO_VOLTAGE_LOGIC "voltageLogic"
#define JSON_DIO_PINS "pins"
#define JSON_DIO_PIN "pin"
#define JSON_DIO_PIN_NUMBER "number"
#define JSON_DIO_PIN_MODE "mode"
#define JSON_DIO_PIN_VALUE "value"
#define JSON_THERMO_ENABLE "enable"
#define JSON_THERMO_DURATION "duration"
#define JSON_THERMO_TYPE "type"
#define JSON_THERMO_TEMPERATURE "temperature"
#define JSON_THERMO_TEMPERATURE_CHANGE_RATE "maxTemperatureChangeRate"

#define JSON_KEY_CHIPSNAME "chipsName"
#define JSON_KEY_CHIPSBATCH "chipsBatch"
#define JSON_KEY_POWERSUPPLIES "powerSupplies"
#define JSON_KEY_GENERATOR "generator"
#define JSON_KEY_THERMOCOUPLE "thermocouple"
#define JSON_KEY_SMU "smu"
#define JSON_KEY_DIO "dio"
#define JSON_KEY_PSNUMBER "number"
#define JSON_KEY_PSSCHEDULE "schedule"
#define JSON_KEY_THERMOPROFILE "thermoProfile"

namespace GlobalDefinitions
{
    Q_NAMESPACE

    enum DataFormPageType
		{
			  THERMO,
			  PXI
		};
		Q_ENUM_NS(DataFormPageType)

    enum ItemType {
        ITEM_NONE,
        ITEM_SLOT,
        ITEM_POWER_SUPPLY,
        ITEM_GENERATOR,
        ITEM_DIO,
        ITEM_THERMOCOUPLE,
        ITEM_SMU
    };
    Q_ENUM_NS(ItemType)

    enum GenFunction {
        SINUS,
        SQUARE,
        TRIANGLE,
        RAMP
    };
    Q_ENUM_NS(GenFunction)

    enum PinMode {
        NONE,
        READ,
        WRITE
    };
    Q_ENUM_NS(PinMode)

    enum ThermoMode {
        COOLING = -1,
        MAINTAINING = 0,
        HEATING = 1
    };
    Q_ENUM_NS(ThermoMode)

    enum BlockPosition {
        BEFORE=-1,
        AFTER=1
    };
    Q_ENUM_NS(BlockPosition)
};


#endif // GLOBAL_DEFINITIONS_H

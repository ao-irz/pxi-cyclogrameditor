#include "thermodurationwidget.h"

#include <QPainter>
#include <QLabel>

ThermoDurationWidget::ThermoDurationWidget(QWidget *parent) : QWidget(parent)
{
  setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  setFixedSize(THERMO_FRAME_WIDTH,THERMO_FRAME_HEIGHT);
  setContentsMargins(0,0,0,0);

  setAutoFillBackground(true);
  setAttribute(Qt::WA_StyledBackground, true);
  setStyleSheet("background-color: white;");

  QLabel* temperatureMarkLabel;
  int labelY = THERMO_FRAME_GRID_TOP_MARGIN;
  QFont f;
  f.setPointSize(9);
  int redColorValue = 220;
  int blueColorValue = 0;
  for(int temperatureStep=OVEN_MAX_TEMPERATURE; temperatureStep>=OVEN_MIN_TEMPERATURE; temperatureStep-=10)
  {
    if ( temperatureStep%30==0 )
    {
      temperatureMarkLabel = new QLabel(QString::number(temperatureStep)+"°",this);
      temperatureMarkLabel->setFont(f);
      temperatureMarkLabel->setStyleSheet(QString::asprintf("color: rgb(%d,0,%d);",redColorValue,blueColorValue));
      temperatureMarkLabel->setContentsMargins(0,0,0,0);
      temperatureMarkLabel->setAlignment(Qt::AlignVCenter|Qt::AlignRight);
      temperatureMarkLabel->setGeometry(1,labelY-5,25,10);
    }
    labelY+=THERMO_FRAME_GRID_LINE_STEP;
    redColorValue-=10;
    blueColorValue+=10;
  }

}

void ThermoDurationWidget::paintEvent(QPaintEvent *event)
{
  Q_UNUSED(event)

  QPainter qp(this);
  QPen pen(Qt::gray);
  pen.setWidth(1);
  int lineY = THERMO_FRAME_GRID_TOP_MARGIN;
  for(int temperatureStep=OVEN_MAX_TEMPERATURE; temperatureStep>=OVEN_MIN_TEMPERATURE; temperatureStep-=10)
  {
    if ( temperatureStep%30!=0 )
      pen.setStyle(Qt::DotLine);
    else
      pen.setStyle(Qt::DashLine);
    qp.setPen(pen);
    qp.drawLine(THERMO_FRAME_GRID_LEFT_MARGIN, lineY,
                THERMO_FRAME_GRID_LEFT_MARGIN+THERMO_FRAME_GRID_WIDTH, lineY);
    lineY+=THERMO_FRAME_GRID_LINE_STEP;
  }
}

void ThermoDurationWidget::drawThemperatureGrid()
{
}

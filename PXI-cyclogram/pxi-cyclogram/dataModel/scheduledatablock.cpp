#include "scheduledatablock.h"

ScheduleDataBlock::ScheduleDataBlock(): QJsonObject()
  //,m_enDataItemType(GlobalDefinitions::ItemType::ITEM_NONE)
  ,m_strDuration("0")
  ,m_nDuration(0)
  ,m_bEnable(false)
  ,m_bResizingFlag(false)
{
  setDuration(24*60*60*1000);// 24 часа
  //setDuration(164046021);
}

ScheduleDataBlock::~ScheduleDataBlock()
{
}

QString ScheduleDataBlock::durationStr() const
{
  return m_bResizingFlag ? m_strTempDuration : m_strDuration;
}

quint64 ScheduleDataBlock::durationInt() const
{
  return m_bResizingFlag ? m_nTempDuration : m_nDuration;
}

void ScheduleDataBlock::setDuration(const QString duration)
{
  if ( m_bResizingFlag )
  {
    m_strTempDuration = duration;
    m_nTempDuration = m_strDuration.toULongLong();
  }
  else
  {
    m_strDuration = duration;
    m_nDuration = m_strDuration.toULongLong();
    insert("duration",QJsonValue(m_strDuration));
  }
}

void ScheduleDataBlock::setDuration(const quint64 duration)
{
  if ( m_bResizingFlag )
  {
    m_nTempDuration = duration;
    m_strTempDuration = QString::number( m_nDuration );
  }
  else
  {
    m_nDuration = duration;
    m_strDuration = QString::number( m_nDuration );
    insert("duration",QJsonValue(m_strDuration));
  }
}

void ScheduleDataBlock::setEnable(const bool bEnabled)
{
  m_bEnable = bEnabled;
}

bool ScheduleDataBlock::isEnabled()
{
  return m_bEnable;
}

void ScheduleDataBlock::recalcDuration(float percent)
{
  if ( percent>0 )
    m_nTempDuration = static_cast<quint64>(m_nDuration*percent);

  setDuration(static_cast<quint64>(m_nTempDuration));

}

void ScheduleDataBlock::setResizing(bool bResizing)
{
  m_bResizingFlag = bResizing;

  if ( m_bResizingFlag )
    setDuration(m_nDuration);
  else
    setDuration(m_nTempDuration);
}

#include "pxiswitcher.h"
#include "pxidetailswidget.h"

PxiSwitcher::PxiSwitcher(PxiDetailsWidget *parent): QSlider(Qt::Orientation::Vertical,parent)
,m_bStateOn(false)
,m_pParent(parent)
{
  m_nSwitcherStyle = SwitcherStyle::DEVICE_SWITCHER;
  QString strSliderStyle;
  QSize switcherSize;
  switcherSize.setWidth(32);
  switcherSize.setHeight(32);
  strSliderStyle = "QSlider::groove:vertical { \
              width:6px; \
              border: 1px solid darkblue;\
              background: darkblue;\
              border-radius: 3px;\
          } \
          QSlider::handle:vertical {  \
              background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 red, stop:0.7 #2eb82e); \
              border: 2px solid lightgray;  \
              height: 12px; \
              margin-top: -1px;\
              margin-bottom: -1px;\
              margin-left: -7px;\
              margin-right: -7px;\
              border-radius: 5px;\
          } \
          QSlider::handle:vertical:disabled {  \
              background-color: lightgray; \
              border: 2px solid lightgray;  \
              height: 12px; \
              margin-top: -1px;\
              margin-bottom: -1px;\
              margin-left: -7px;\
              margin-right: -7px;\
              border-radius: 5px;\
          } \
          QSlider::add-page:vertical { background-color: #2eb82e; border-radius: 3px;} \
          QSlider::sub-page:vertical { background-color: red;  border-radius: 3px;}\
          QSlider::add-page:vertical:disabled { background-color: lightgray; border: 1px solid lightgray; border-radius: 3px;} \
          QSlider::sub-page:vertical:disabled { background-color: lightgray; border: 1px solid lightgray; border-radius: 3px;}";

  setStyleSheet(strSliderStyle);

  setMaximumSize(switcherSize);
  setMinimumSize(switcherSize);
  setMinimum(0);
  setMaximum(1);
  setSingleStep(1);
  setPageStep(1);
  setValue(m_bStateOn?1:0);
}

PxiSwitcher::PxiSwitcher(QWidget *parent): QSlider(Qt::Orientation::Horizontal,parent)
,m_bStateOn(false)
,m_pParent(parent)
{
  m_nSwitcherStyle = SwitcherStyle::DIO_SWITCHER;
  QSize switcherSize;

  switcherSize.setWidth(24);
  switcherSize.setHeight(16);

  setMinimum(0);
  setMaximum(1);
  setSingleStep(1);
  setPageStep(1);
  setValue(m_bStateOn?1:0);
}

void PxiSwitcher::setState(bool bState)
{
  if ( m_bStateOn!=bState )
  {
    m_bStateOn = bState;
    setValue(m_bStateOn?1:0);
    emit valueChanged();
  }
}

bool PxiSwitcher::valueBool() const
{
  return m_bStateOn;
}

void PxiSwitcher::mousePressEvent(QMouseEvent *ev)
{
    Q_UNUSED(ev)

    setState( !m_bStateOn ) ;

    if ( m_nSwitcherStyle==SwitcherStyle::DEVICE_SWITCHER )
      emit static_cast<PxiDetailsWidget*>(m_pParent)->stateChangedSignal(m_bStateOn);
}

#include "thermodataform.h"
#include "../mainwindow.h"

#include <cmath>
#include <QMessageBox>
#include <QMetaEnum>

ThermoDataForm::ThermoDataForm(QWidget *parent) : DataForm(parent)
  ,m_pActiveSection(nullptr)
  ,m_nActiveSectionIndex(-1)
  ,m_bNoSave(false)
{
  m_nDataFormType = GlobalDefinitions::DataFormPageType::THERMO;

  setupForm();

  connect(this,&DataForm::resetDataSignal,this,&ThermoDataForm::clearFormSlot);
}

ThermoScheduleSection* ThermoDataForm::section(int sectionIdx)
{
  ThermoScheduleSection* theSection = nullptr;
  if ( m_lstSections.count()>0 && sectionIdx>=0 && sectionIdx<m_lstSections.count() )
    theSection = m_lstSections[sectionIdx];

  return theSection;
}

bool ThermoDataForm::canClose(QString msgTitle)
{
  Q_UNUSED(msgTitle)

  return DataForm::canClose("Термо-циклограмма");
}

void ThermoDataForm::setupForm()
{
  DataForm::setupForm();

  m_pChipsNameLabel->setText("");
  m_pChipsBatchLabel->setText("");
  m_pChipsNameEdit->setFixedWidth(203);
  m_pChipsBatchEdit->hide();

  m_pDataGridLayout->setHorizontalSpacing(2);
  m_pDataGridLayout->setColumnStretch(0,0);
  m_pDataGridLayout->setColumnStretch(1,0);
  m_pDataGridLayout->setColumnStretch(2,1);

  m_pDurationWidget = new ThermoDurationWidget(this);
  m_pDataGridLayout->addWidget(m_pDurationWidget,m_nSetupCurrentRow,0,1,3);
  m_nSetupCurrentRow++;

  m_pHSeparator = new QFrame(this);
  m_pHSeparator->setFrameShape(QFrame::HLine);
  m_pHSeparator->setFrameShadow(QFrame::Sunken);
  m_pDataGridLayout->addWidget(m_pHSeparator,m_nSetupCurrentRow,0,1,3);
  m_nSetupCurrentRow++;

  setupDetailsArea();

  updateDetailsArea();
}

void ThermoDataForm::setupDetailsArea()
{
  m_pDetailsWidget = new PxiDetailsWidget("Термокамера",this, false);
  m_pDataGridLayout->addWidget(m_pDetailsWidget,m_nSetupCurrentRow++,0,1,3);

  connect(this,&ThermoDataForm::dataRowBlocksCountChangedSignal,
          m_pDetailsWidget,&PxiDetailsWidget::dataRowBlocksCountChangedSlot);
  /*connect(m_pDetailsWidget,&PxiDetailsWidget::stateChangedSignal,
          this,&PxiDeviceDataRow::switchStateChangedSlot);*/
  connect(m_pDetailsWidget,&PxiDetailsWidget::addNewDataBlockBeforeSignal,
          [this](){addNewDataBlock(GlobalDefinitions::BlockPosition::BEFORE);});
  connect(m_pDetailsWidget,&PxiDetailsWidget::addNewDataBlockAfterSignal,
          [this](){addNewDataBlock(GlobalDefinitions::BlockPosition::AFTER);});
  connect(m_pDetailsWidget,&PxiDetailsWidget::deleteDataBlockSignal,
          this,&ThermoDataForm::deleteDataBlockSlot);

  QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();
  m_pDetailsWidget->switcher()->setProperty( JSON_FIELD_NAME, JSON_THERMO_ENABLE );

  m_nSetupCurrentColumn = 2;
  int nCurrentRow = 0;

  connect(m_pDetailsWidget->switcher(),&PxiSwitcher::valueChanged,
          this,&ThermoDataForm::switcherValueChangedSlot);

  m_pDurationControlsWidget = new PxiDurationControlsWidget(m_pDetailsWidget);
  m_pDurationControlsWidget->setProperty( JSON_FIELD_NAME, JSON_THERMO_DURATION );
  pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Продолжительность (ч:м:с)",m_pDurationControlsWidget),
                     nCurrentRow++,m_nSetupCurrentColumn);
  connect(m_pDurationControlsWidget,&PxiDurationControlsWidget::durationChangedSignal,
          this,&ThermoDataForm::durationChangedSlot);

  m_pThermoTypeBox = new QComboBox(m_pDetailsWidget);
  m_pThermoTypeBox->setProperty( JSON_FIELD_NAME, JSON_THERMO_TYPE );
  m_pThermoTypeBox->setIconSize(QSize(64,18));
  m_pThermoTypeBox->addItem( QIcon(HEATING_ICON), "", GlobalDefinitions::ThermoMode::HEATING );
  m_pThermoTypeBox->addItem( QIcon(MAINTAINING_ICON), "", GlobalDefinitions::ThermoMode::MAINTAINING );
  m_pThermoTypeBox->addItem( QIcon(COOLING_ICON), "", GlobalDefinitions::ThermoMode::COOLING );
  connect(m_pThermoTypeBox,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&ThermoDataForm::thermoTypeChangedSlot);
  QHBoxLayout* hLayout = new QHBoxLayout();
  QLabel* l = new QLabel( "Режим", m_pDetailsWidget );
  hLayout->addWidget(l);
  hLayout->addWidget(m_pThermoTypeBox);
  pLayout->addLayout(hLayout,nCurrentRow++,m_nSetupCurrentColumn,Qt::AlignLeft);

  m_pTemperatureEdit = new QDoubleSpinBox(m_pDetailsWidget);
  m_pTemperatureEdit->setProperty( JSON_FIELD_NAME, JSON_THERMO_TEMPERATURE );
  m_pTemperatureEdit->setMaximum(OVEN_MAX_TEMPERATURE);
  m_pTemperatureEdit->setMinimum(OVEN_MIN_TEMPERATURE);
  m_pTemperatureEdit->setDecimals(1);
  m_pTemperatureEdit->setSingleStep(0.1);
  pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Температура, °C",m_pTemperatureEdit),
                     nCurrentRow++,m_nSetupCurrentColumn);
  connect(m_pTemperatureEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,&ThermoDataForm::temperatureChangedSlot);

  m_pTemperatureChangeRateEdit = new QDoubleSpinBox(m_pDetailsWidget);
  m_pTemperatureChangeRateEdit->setProperty( JSON_FIELD_NAME, JSON_THERMO_TEMPERATURE_CHANGE_RATE );
  m_pTemperatureChangeRateEdit->setMaximum(100);
  m_pTemperatureChangeRateEdit->setDecimals(1);
  m_pTemperatureChangeRateEdit->setSingleStep(0.1);
  pLayout->addLayout(m_pDetailsWidget->createCheckableFieldVLayout("Скорость изменения температуры, °C/мин",m_pTemperatureChangeRateEdit,m_pTemperatureChangeRateCheck),
                     nCurrentRow++,m_nSetupCurrentColumn);
  connect(m_pTemperatureChangeRateEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,&ThermoDataForm::temperatureRateValueChangedSlot);
  connect(m_pTemperatureChangeRateCheck,&QCheckBox::stateChanged,
          this,&ThermoDataForm::temperatureRateCheckStateChangedSlot);

  pLayout->addItem(new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Expanding),
                             nCurrentRow,m_nSetupCurrentColumn);

  pLayout->addWidget(m_pDetailsWidget->separatorFrame(),0,1,nCurrentRow,1);

  m_pThermoTypeBox->setCurrentIndex(1);
  m_pThermoTypeBox->currentIndexChanged(1);

  emit dataRowBlocksCountChangedSignal(sectionsCount());
}

void ThermoDataForm::thermoTypeChangedSlot(int idx)
{
  Q_UNUSED(idx)

  GlobalDefinitions::ThermoMode thermoMode = static_cast<GlobalDefinitions::ThermoMode>(m_pThermoTypeBox->currentData().toInt());
  switch ( thermoMode)
  {
    case GlobalDefinitions::ThermoMode::COOLING:
    {
      m_pThermoTypeBox->setToolTip( THERMO_COOLING_TOOLTIP );
      m_pTemperatureChangeRateCheck->setEnabled(true);
      break;
    }
    case GlobalDefinitions::ThermoMode::MAINTAINING:
    {
      m_pThermoTypeBox->setToolTip( THERMO_MAINTAINING_TOOLTIP );
      m_pTemperatureChangeRateCheck->setEnabled(false);
      m_pTemperatureChangeRateCheck->setChecked(false);
      m_pTemperatureChangeRateEdit->setValue(0);
      break;
    }
    case GlobalDefinitions::ThermoMode::HEATING:
    {
      m_pThermoTypeBox->setToolTip( THERMO_HEATING_TOOLTIP );
      m_pTemperatureChangeRateCheck->setEnabled(true);
      break;
    }
  }

  if ( m_pActiveSection && !m_bNoSave )
  {
    float fTemperatureDiff =  roundf((m_pActiveSection->initialTemperature()-m_pActiveSection->finalTemperature())/2*10)/10;
    m_pActiveSection->setThermoMode(thermoMode);
    m_pActiveSection->setTemperature(m_pActiveSection->finalTemperature()+fTemperatureDiff);
    redrawBars();
  }
}

void ThermoDataForm::switcherValueChangedSlot()
{
  if ( m_pActiveSection )
    m_pActiveSection->setEnable(m_pDetailsWidget->switcher()->valueBool());

  updateDetailsArea();
}

void ThermoDataForm:: updateDetailsArea()
{
  m_bNoSave = true;

  if ( m_pActiveSection )
  {
    m_pDetailsWidget->switcher()->setState(m_pActiveSection->isEnabled());

    m_pDurationControlsWidget->setEnabled(true);
    m_pDurationControlsWidget->setDuration(m_pActiveSection->dataBlock()->durationInt());

    if ( m_pActiveSection->isEnabled() )
    {
      m_pThermoTypeBox->setEnabled(true);
      for(int i=0; i<m_pThermoTypeBox->count(); i++)
      {
        if ( static_cast<GlobalDefinitions::ThermoMode>(m_pThermoTypeBox->itemData(i).toInt())==m_pActiveSection->thermoMode() )
        {
          m_pThermoTypeBox->setCurrentIndex(i);
          break;
        }
      }

      m_pTemperatureEdit->setEnabled(true);
      m_pTemperatureEdit->setValue(static_cast<double>(m_pActiveSection->finalTemperature()));

      m_pTemperatureChangeRateCheck->setEnabled(m_pActiveSection->thermoMode()!=GlobalDefinitions::ThermoMode::MAINTAINING);
      if ( m_pActiveSection->dataBlock()->value(JSON_THERMO_TEMPERATURE_CHANGE_RATE) == QJsonValue::Undefined )
      {
         m_pTemperatureChangeRateCheck->setChecked(false);
         m_pTemperatureChangeRateEdit->setValue(0);
      }
      else
      {
        m_pTemperatureChangeRateCheck->setChecked(true);
        m_pTemperatureChangeRateEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_THERMO_TEMPERATURE_CHANGE_RATE).toDouble());
      }
    }
    else
    {
      m_pThermoTypeBox->setEnabled(false);
      m_pTemperatureEdit->setEnabled(false);
      m_pTemperatureChangeRateCheck->setChecked(false);
      m_pTemperatureChangeRateCheck->setEnabled(false);
      m_pTemperatureChangeRateEdit->setValue(0);
    }

  }
  else
  {
    m_pDetailsWidget->switcher()->setEnabled(false);
    m_pDurationControlsWidget->setEnabled(false);
    m_pThermoTypeBox->setEnabled(false);
    m_pTemperatureEdit->setEnabled(false);
    m_pTemperatureChangeRateCheck->setChecked(false);
    m_pTemperatureChangeRateCheck->setEnabled(false);
  }

  m_bNoSave = false;
}

int ThermoDataForm::sectionsCount() const
{
  return m_lstSections.count();
}

void ThermoDataForm::addNewDataBlock(GlobalDefinitions::BlockPosition nBlockPosition)
{
  m_pActiveSection = new ThermoScheduleSection(this,m_pDurationWidget);
  switch(nBlockPosition)
  {
    case GlobalDefinitions::BlockPosition::BEFORE:
    {
      if ( m_nActiveSectionIndex<0 )
        m_nActiveSectionIndex++;
      break;
    }
    case GlobalDefinitions::BlockPosition::AFTER:
    {
      m_nActiveSectionIndex++;
    }
  }
  m_pActiveSection->addDataBlock(new ScheduleDataBlock());

  m_lstSections.insert(m_nActiveSectionIndex,m_pActiveSection);
  for(int i=0; i<m_lstSections.count(); i++)
    m_lstSections[i]->setSectionIndex(i);

  //m_pActiveSection инициализируется в addNewDataBlock...Slot

  ThermoDurationBar* newBar = new ThermoDurationBar(m_pDurationWidget,m_pActiveSection);

  newBar->setGeometry( THERMO_FRAME_GRID_LEFT_MARGIN+1+(THERMO_DURATION_BLOCK_DEFAULT_WIDTH-1)*(m_lstSections.count()-1),
                       THERMO_FRAME_GRID_TOP_MARGIN,
                       1,1 );
  newBar->show();

  clearDetailsWidget();
  storeDataControlContents();

  connect( newBar, &ThermoDurationBar::startResizeDurationSignal, this, &ThermoDataForm::startResizeDurationSlot );
  connect( newBar, &ThermoDurationBar::resizingDurationSignal, this, &ThermoDataForm::resizingDurationSlot );
  connect( newBar, &ThermoDurationBar::stopResizeDurationSignal, this, &ThermoDataForm::stopResizeDurationSlot );
  connect( newBar, &ThermoDurationBar::changingTemperatureSignal, this, &ThermoDataForm::changingTemperatureSlot );

  connect(m_pActiveSection,&ThermoScheduleSection::selectedSignal,
          this,&ThermoDataForm::sectionSelectedSlot);
  connect(this,&ThermoDataForm::temperatureChangedSignal,
          m_pActiveSection,&ThermoScheduleSection::temperatureChangedSlot);

  if ( m_lstSections.count()==1 )
    m_pActiveSection->setTemperature(OVEN_DEFAULT_TEMPERATURE);
  else
  {
    if ( m_nActiveSectionIndex==0 )
      m_pActiveSection->setTemperature(m_lstSections[m_nActiveSectionIndex+1]->initialTemperature());
    else
      m_pActiveSection->setTemperature(m_lstSections[m_nActiveSectionIndex-1]->finalTemperature());
  }

  redrawBars();

  //updateDetailsArea();
  emit dataRowBlocksCountChangedSignal(sectionsCount());

  m_pActiveSection->select();
}

void ThermoDataForm::redrawBars()
{
  // посчитать общую длительность всех блоков
  quint64 nTotalDuration = 0;
  for(auto& section : m_lstSections )
    nTotalDuration += section->dataBlock()->durationInt();

  // вычислить предполагаемую ширину блоков в пикселях,
  // с учетом назначения блокам со слишком малой относительной длительностью
  // минимальной ширины в THERMO_DURATION_BLOCK_MINIMUM_WIDTH пикселей
  // и посчитать потенциальную суммарную ширину всех блоков
  QList<int> lstBlocksWidths;
  int nTotalRealBlocksWidth=0;
  for(auto& section : m_lstSections )
  {
    double blockDurationPercentage = static_cast<double>(section->dataBlock()->durationInt())/nTotalDuration;
    int barWidth = static_cast<int>(round(static_cast<double>(THERMO_DURATION_BLOCKS_TOTAL_WIDTH) * blockDurationPercentage));
    if ( barWidth<THERMO_DURATION_BLOCK_MINIMUM_WIDTH )
      barWidth = THERMO_DURATION_BLOCK_MINIMUM_WIDTH;
    lstBlocksWidths.append(barWidth);
    nTotalRealBlocksWidth += barWidth;
  }

  // выставить реальную ширину каждого блока,
  // отталкиваясь от процентного соотношения предполагаемой длины блока к суммарной теоретической шириной всех блоков
  // и области окна, отведенной для отрисовки циклограммы
  int leftBlockMargin = THERMO_FRAME_GRID_LEFT_MARGIN+1;
  for( int i=0; i<m_lstSections.count(); i++ )
  {
    double blockDurationPercentage = static_cast<double>(lstBlocksWidths[i])/nTotalRealBlocksWidth;
    int barWidth = static_cast<int>(round(static_cast<double>(THERMO_DURATION_BLOCKS_TOTAL_WIDTH) * blockDurationPercentage));
    if ( barWidth<THERMO_DURATION_BLOCK_MINIMUM_WIDTH )
      barWidth = THERMO_DURATION_BLOCK_MINIMUM_WIDTH;
    m_lstSections[i]->bar()->setGeometry(leftBlockMargin, THERMO_FRAME_GRID_TOP_MARGIN,
                                         barWidth,THERMO_DURATION_BLOCK_DEFAULT_HEIGHT);
    m_lstSections[i]->bar()->repaint();
    leftBlockMargin += barWidth-1;
  }
}

void ThermoDataForm::durationChangedSlot()
{
  if ( m_pActiveSection )
  {
    m_pActiveSection->dataBlock()->setDuration(m_pDurationControlsWidget->durationInt());
    redrawBars();
  }
}

void ThermoDataForm::temperatureChangedSlot(double temperatureValue)
{
  if ( !m_pActiveSection || m_bNoSave )
    return;

  m_pActiveSection->setTemperature(static_cast<float>(temperatureValue));

  redrawBars();
}

void ThermoDataForm::temperatureRateValueChangedSlot(double temperatureChangeRate)
{
  if ( !m_pActiveSection || m_bNoSave )
    return;

  if ( m_pTemperatureChangeRateCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(QString(JSON_THERMO_TEMPERATURE_CHANGE_RATE),QJsonValue(round(temperatureChangeRate*10)/10));
  else
    m_pActiveSection->dataBlock()->remove(JSON_THERMO_TEMPERATURE_CHANGE_RATE);
}

void ThermoDataForm::temperatureRateCheckStateChangedSlot(int state)
{
  Q_UNUSED(state)

  if ( !m_pActiveSection || m_bNoSave )
    return;

  if ( m_pTemperatureChangeRateCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(JSON_THERMO_TEMPERATURE_CHANGE_RATE,QJsonValue(round(m_pTemperatureChangeRateEdit->value()*10)/10));
  else
    m_pActiveSection->dataBlock()->remove(JSON_THERMO_TEMPERATURE_CHANGE_RATE);
}


void ThermoDataForm::deleteDataBlockSlot()
{
  if ( m_pActiveSection )
  {
    m_lstSections.removeAt(m_nActiveSectionIndex);
    delete m_pActiveSection;

    if ( m_nActiveSectionIndex>=m_lstSections.count() )
      m_nActiveSectionIndex--;

    if ( m_nActiveSectionIndex>=0 )
    {
      for(int i=0; i<m_lstSections.count(); i++)
        m_lstSections[i]->setSectionIndex(i);

      if ( m_nActiveSectionIndex>0)
        section(m_nActiveSectionIndex-1)->setTemperature(section(m_nActiveSectionIndex-1)->finalTemperature());

      m_pActiveSection = m_lstSections[m_nActiveSectionIndex];
      m_pActiveSection->select();
    }
    else
    {
      m_pActiveSection = nullptr;
      clearDetailsWidget();
    }
  }

  redrawBars();

  updateDetailsArea();
  emit dataRowBlocksCountChangedSignal(sectionsCount());
}

void ThermoDataForm::clearDetailsWidget()
{
  m_bNoSave = true;

  if ( m_pActiveSection )
    m_pDurationControlsWidget->setDuration(m_pActiveSection->dataBlock()->durationInt());
  else
  {
    m_pDetailsWidget->switcher()->setState(false);
    m_pDurationControlsWidget->clear();
  }

  m_pThermoTypeBox->setCurrentIndex(1);
  m_pTemperatureEdit->setValue(OVEN_DEFAULT_TEMPERATURE);
  m_pTemperatureChangeRateEdit->setValue(0);
  m_pTemperatureChangeRateCheck->setChecked(false);

  m_bNoSave = false;
}

void ThermoDataForm::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  //m_pActiveSection->dataBlock()->insert(JSON_THERMO_ENABLE,QJsonValue(true));
  m_pActiveSection->setEnable(true);
  m_pActiveSection->dataBlock()->insert(JSON_THERMO_DURATION,QJsonValue(m_pDurationControlsWidget->durationStr()));

  GlobalDefinitions::ThermoMode thermoMode = static_cast<GlobalDefinitions::ThermoMode>(m_pThermoTypeBox->currentData().toInt());
  m_pActiveSection->dataBlock()->insert(JSON_THERMO_TYPE,QJsonValue(QVariant::fromValue(thermoMode).toString().toLower()));
  m_pActiveSection->setThermoMode(thermoMode);

  if ( m_pTemperatureChangeRateCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(JSON_THERMO_TEMPERATURE_CHANGE_RATE,QJsonValue(round(m_pTemperatureChangeRateEdit->value()*10)/10));
  else
    m_pActiveSection->dataBlock()->remove(JSON_THERMO_TEMPERATURE_CHANGE_RATE);
}

void ThermoDataForm::startResizeDurationSlot()
{
  if ( m_pActiveSection )
    m_pActiveSection->dataBlock()->setResizing(true);
}

void ThermoDataForm::resizingDurationSlot( float percent )
{
  m_pActiveSection->dataBlock()->recalcDuration( percent );
  updateDetailsArea();
  redrawBars();
}

void ThermoDataForm::stopResizeDurationSlot()
{
  if ( m_pActiveSection )
    m_pActiveSection->dataBlock()->setResizing(false);
}

void ThermoDataForm::changingTemperatureSlot( int delta )
{
  if ( m_pActiveSection )
  {
    float newTemperature = m_pActiveSection->finalTemperature()+(float)delta*0.3f;
    if ( newTemperature > OVEN_MAX_TEMPERATURE )
      newTemperature = OVEN_MAX_TEMPERATURE;
    if ( newTemperature < OVEN_MIN_TEMPERATURE )
      newTemperature = OVEN_MIN_TEMPERATURE;

    m_pActiveSection->setTemperature(newTemperature);
    updateDetailsArea();
    redrawBars();
  }
}

void ThermoDataForm::sectionSelectedSlot()
{
  ThermoScheduleSection* senderSection = qobject_cast<ThermoScheduleSection*>(sender());
  m_pActiveSection = senderSection;

  for (int idx=0; idx<m_lstSections.count(); idx++)
  {
    if ( m_lstSections[idx]!=m_pActiveSection )
      m_lstSections[idx]->unselect();
    else
      m_nActiveSectionIndex = idx;
  }

  updateDetailsArea();
}

bool ThermoDataForm::checkJson( QString& errMsg )
{
  bool retVal = true;

  QJsonObject rootObject = m_jsonDataLoaded.object();
  if ( !rootObject.contains(JSON_KEY_THERMOPROFILE)  )
  {
    errMsg = "Возможно файл поврежден, или не содержит данных термо-циклограммы!";
    retVal = false;
  }

  return retVal;
}

void ThermoDataForm::distributeData(QString importFileName)
{
  QJsonObject rootObject = m_jsonDataLoaded.object();

  m_pChipsNameEdit->setText(importFileName.section(".",0,0));

  QJsonArray thermoProfileArray = rootObject[JSON_KEY_THERMOPROFILE].toArray();
  if ( thermoProfileArray.count()>0 )
  {
    for( auto sectionElement: thermoProfileArray )
    {
      addNewDataBlock(GlobalDefinitions::BlockPosition::AFTER);

      QJsonObject sectionObject = sectionElement.toObject();
      for( auto dataKey: sectionObject.keys() )
      {
        m_pActiveSection->dataBlock()->insert(dataKey,sectionObject.value(dataKey));
        if ( dataKey==JSON_THERMO_DURATION )
          m_pActiveSection->dataBlock()->setDuration(sectionObject.value(dataKey).toString());
        if ( dataKey==JSON_THERMO_ENABLE )
          m_pActiveSection->setEnable(sectionObject.value(dataKey).toBool());
        if ( dataKey==JSON_THERMO_TEMPERATURE )
        {
          if ( m_pActiveSection->sectionIndex()==0 )
            m_pActiveSection->setTemperature(static_cast<float>(sectionObject.value(dataKey).toDouble()),OVEN_DEFAULT_TEMPERATURE);
          else
            m_pActiveSection->setTemperature(static_cast<float>(sectionObject.value(dataKey).toDouble()),m_lstSections[m_pActiveSection->sectionIndex()-1]->finalTemperature());
        }
        if ( dataKey==JSON_THERMO_TYPE )
        {
          auto&& metaEnum = QMetaEnum::fromType<GlobalDefinitions::ThermoMode>();
          char* keyCharValue = sectionObject.value(dataKey).toString().toUpper().toLocal8Bit().data();
          GlobalDefinitions::ThermoMode thermoMode = static_cast<GlobalDefinitions::ThermoMode>(metaEnum.keyToValue(keyCharValue));
          m_pActiveSection->setThermoMode(thermoMode);
        }
      }
    }
    redrawBars();
    m_lstSections[0]->select();
  }

}

void ThermoDataForm::collectData()
{
  m_jsonDataEditing = QJsonDocument();

  QJsonObject rootObject = QJsonObject();
  QJsonArray thermoProfileArray;

  bool lastStateSwitchedOn = false;
  for(auto& section : m_lstSections)
  {
    if ( section->dataBlock()->value(JSON_THERMO_ENABLE).toBool() )
      thermoProfileArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
    else
    {
      QJsonObject tmpObj;
      tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
      tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
      thermoProfileArray.append(tmpObj);
    }
    lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
  }
  if ( lastStateSwitchedOn )
  {
    QJsonObject tmpObj;
    tmpObj[JSON_ENABLE] = false;
    tmpObj[JSON_DURATION] = "0";
    thermoProfileArray.append(tmpObj);
  }
  if ( !thermoProfileArray.isEmpty() )
    rootObject[JSON_KEY_THERMOPROFILE] = thermoProfileArray;

  if ( !rootObject.isEmpty() )
    m_jsonDataEditing.setObject(rootObject);
}

void ThermoDataForm::clearFormSlot()
{
  while(!m_lstSections.isEmpty())
    deleteDataBlockSlot();
}

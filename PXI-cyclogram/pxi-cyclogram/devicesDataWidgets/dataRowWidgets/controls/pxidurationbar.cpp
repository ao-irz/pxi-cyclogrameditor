#include "pxidurationbar.h"
#include "../pxischedulesection.h"


PxiDurationBar::PxiDurationBar(QWidget* parent,PxiScheduleSection* pParentSection) : QFrame(parent)
  ,m_pParent(parent)
  ,m_pParentSection(pParentSection)
  ,m_bCanResize(false)
  ,m_bCanAcceptWheel(false)
  ,m_bResizing(false)
  ,m_bSelected(false)
  ,m_bStateOn(false)
{
  m_sizeHorCursor = QCursor(Qt::SizeHorCursor);
  m_defaultCursor = QCursor(Qt::ArrowCursor);

  setMouseTracking(true);

  setFrameStyle(QFrame::Box);

  m_strStyleOnSelected = "background-color: rgb(0, 200, 0);";
  m_strStyleOffSelected = "background-color: rgb(255, 102, 51);";
  m_strStyleOnUnselected = "background-color: rgb(155, 192, 155); \
                             background-image: url(:/images/dense5patternGreen.png);";
  m_strStyleOffUnselected = "background-color: rgb(255, 192, 192); \
                              background-image: url(:/images/dense5patternRed.png);";

  setStyleSheet(m_strStyleOffUnselected);

}

void PxiDurationBar::select()
{
  m_bSelected = true;

  if ( m_bStateOn )
    setStyleSheet(m_strStyleOnSelected);
  else
    setStyleSheet(m_strStyleOffSelected);

}

void PxiDurationBar::unselect()
{
  m_bSelected = false;

  if ( m_bStateOn )
    setStyleSheet(m_strStyleOnUnselected);
  else
    setStyleSheet(m_strStyleOffUnselected);
}

void PxiDurationBar::setOnState(bool bState)
{
  m_bStateOn = bState;

  if ( m_bSelected )
    select();
  else
    unselect();
}

void PxiDurationBar::mouseMoveEvent(QMouseEvent *e)
{
  m_bCanAcceptWheel = true;

  if ( !m_bResizing )
  {
    if ( e->x()>= width()-5 )
    {
      setCursor(m_sizeHorCursor);
      m_bCanResize = true;
    }
    else
    {
      setCursor(m_defaultCursor);
      m_bCanResize = false;
    }
  }
  else
  {
    if ( e->x()>DURATION_BLOCKS_SPACING )
      emit resizingSignal( static_cast<float>(e->x())/m_nPreviousWidth );
  }
}

void PxiDurationBar::mousePressEvent(QMouseEvent*)
{
  if ( !m_bSelected )
    m_pParentSection->select();

  if ( m_bCanResize )
  {
    m_bResizing = true;
    emit startResizeSignal();
    m_nPreviousWidth = width();
  }
}

void PxiDurationBar::mouseReleaseEvent(QMouseEvent*)
{
  if ( m_bResizing )
  {
    emit stopResizeSignal();
    m_bResizing = false;
  }
}

void PxiDurationBar::wheelEvent(QWheelEvent *e)
{
  m_nPreviousWidth = width();

  QPoint numPixels = e->angleDelta();
  if ( m_bCanAcceptWheel && !numPixels.isNull() )
  {
    if ( !m_bSelected )
      m_pParentSection->select();

    if ( width()>DURATION_BLOCKS_SPACING )
    {
      int delta = numPixels.ry() > 0 ? DURATION_BLOCKS_RESIZE_STEP :
                  numPixels.ry() < 0 ? DURATION_BLOCKS_RESIZE_STEP*-1 : 0 ;
      emit startResizeSignal();
      emit resizingSignal( static_cast<float>(width()+delta)/m_nPreviousWidth );
      emit stopResizeSignal();
    }

    e->accept();
  }
  else
    e->ignore();
}

void PxiDurationBar::leaveEvent(QEvent* e)
{
  Q_UNUSED(e)

  m_bCanAcceptWheel = false;
}

void PxiDurationBar::resizeBar(int newWidth)
{
  if ( newWidth>DURATION_BLOCKS_SPACING )
    setFixedWidth( newWidth );
  else
    setFixedWidth( DURATION_BLOCKS_SPACING );
}

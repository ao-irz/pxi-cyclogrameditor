#include "pxidevicedatarowPS.h"
#include "../../global_definitions.h"

PxiDeviceDataRowPS::PxiDeviceDataRowPS(const int nDeviceIndex,
                                       const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget *parent):
    PxiDeviceDataRow(GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY,
                     nDeviceIndex,
                     strLabelText,
                     normalIcon,
                     selectedIcon,
                     parent)
{
    setupDurationArea();
    setupDetailsArea();
}

void PxiDeviceDataRowPS::clearDetailsWidget()
{
  m_bNoSave = true;

  PxiDeviceDataRow::clearDetailsWidget();

  m_pVoltageEdit->setValue(0);
  m_pVoltageTolerancePlusEdit->setValue(0);
  m_pVoltageToleranceMinusEdit->setValue(0);

  m_pCurrentEdit->setValue(0);
  m_pCurrentTolerancePlusEdit->setValue(0);
  m_pCurrentToleranceMinusEdit->setValue(0);

  m_bNoSave = false;
}

void PxiDeviceDataRowPS::setupDetailsArea()
{
// там выставляются контролы времени, единые для всех
    PxiDeviceDataRow::setupDetailsArea();

    QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();

// блок параметров управления напряжением

    m_pVoltageEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pVoltageEdit->setProperty( JSON_FIELD_NAME ,JSON_VOLTAGE);
    m_pVoltageEdit->setDecimals(3);
    switch( m_nDeviceIndex )
    {
      case 0:
      case 3:
      {
        m_pVoltageEdit->setMaximum(6);
        break;
      }
      case 1:
      {
        m_pVoltageEdit->setMaximum(20);
        break;
      }
      case 2:
      {
        m_pVoltageEdit->setMaximum(20);
        m_pVoltageEdit->setMinimum(-20);
        break;
      }
      default:;
    }

    m_pVoltageEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pVoltageEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    /*connect(m_pVoltageEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::setVoltageToleranceLimitsSlot);*/
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Напряжение (В)",m_pVoltageEdit),
                       0,m_setupControlColumn++);

    m_pVoltageTolerancePlusEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pVoltageTolerancePlusEdit->setProperty( JSON_FIELD_NAME ,JSON_VOLTAGE_TOLERANCE_PLUS);
    m_pVoltageTolerancePlusEdit->setDecimals(3);
    m_pVoltageTolerancePlusEdit->setMaximum(1);
    m_pVoltageTolerancePlusEdit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    m_pVoltageTolerancePlusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pVoltageTolerancePlusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (+)",m_pVoltageTolerancePlusEdit),
                       0,m_setupControlColumn);

    m_pVoltageToleranceMinusEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pVoltageToleranceMinusEdit->setProperty( JSON_FIELD_NAME ,JSON_VOLTAGE_TOLERANCE_MINUS);
    m_pVoltageToleranceMinusEdit->setDecimals(3);
    m_pVoltageToleranceMinusEdit->setMaximum(1);
    m_pVoltageToleranceMinusEdit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    m_pVoltageToleranceMinusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pVoltageToleranceMinusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (-)",m_pVoltageToleranceMinusEdit),
                       1,m_setupControlColumn++);


    //m_pVoltageTolerancePlusEdit->setMaximum(m_pVoltageEdit->maximum());
    //m_pVoltageToleranceMinusEdit->setMinimum(m_pVoltageEdit->minimum());
    //setVoltageToleranceLimitsSlot(m_pVoltageEdit->value());

    pLayout->addWidget(m_pDetailsWidget->separatorFrame(),0,m_setupControlColumn++,3,1);
    // блок параметров управления током

    m_pCurrentEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pCurrentEdit->setProperty( JSON_FIELD_NAME ,JSON_CURRENT);
    m_pCurrentEdit->setDecimals(3);
    switch( m_nDeviceIndex )
    {
      case 1:
      case 2:
      {
        m_pCurrentEdit->setMaximum(2);
        break;
      }
      default:;
        m_pCurrentEdit->setMaximum(1);
    }
    m_pCurrentEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pCurrentEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Ток (А)",m_pCurrentEdit),
                       0,m_setupControlColumn++);

    m_pCurrentTolerancePlusEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pCurrentTolerancePlusEdit->setProperty( JSON_FIELD_NAME ,JSON_CURRENT_TOLERANCE_PLUS);
    m_pCurrentTolerancePlusEdit->setDecimals(3);
    m_pCurrentTolerancePlusEdit->setMaximum(0.1);
    m_pCurrentTolerancePlusEdit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    m_pCurrentTolerancePlusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pCurrentTolerancePlusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (+)",m_pCurrentTolerancePlusEdit),
                       0,m_setupControlColumn);

    m_pCurrentToleranceMinusEdit = new QDoubleSpinBox(m_pDetailsWidget);
    m_pCurrentToleranceMinusEdit->setProperty( JSON_FIELD_NAME ,JSON_CURRENT_TOLERANCE_MINUS);
    m_pCurrentToleranceMinusEdit->setDecimals(3);
    m_pCurrentToleranceMinusEdit->setMaximum(1);
    m_pCurrentToleranceMinusEdit->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    m_pCurrentToleranceMinusEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
    connect(m_pCurrentToleranceMinusEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            this,&PxiDeviceDataRowPS::dataControlValueChangedSlot);
    pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("допуск (-)",m_pCurrentToleranceMinusEdit),
                       1,m_setupControlColumn++);

}

void PxiDeviceDataRowPS::updateDetailsArea()
{
  PxiDeviceDataRow::updateDetailsArea();

  if ( m_pActiveSection )
  {
    m_pVoltageEdit->setEnabled(true);
    m_pVoltageTolerancePlusEdit->setEnabled(true);
    m_pVoltageToleranceMinusEdit->setEnabled(true);

    m_pCurrentEdit->setEnabled(true);
    m_pCurrentTolerancePlusEdit->setEnabled(true);
    m_pCurrentToleranceMinusEdit->setEnabled(true);

    fillDataControlContents();
  }
  else
  {
    m_pVoltageEdit->setEnabled(false);
    m_pVoltageTolerancePlusEdit->setEnabled(false);
    m_pVoltageToleranceMinusEdit->setEnabled(false);

    m_pCurrentEdit->setEnabled(false);
    m_pCurrentTolerancePlusEdit->setEnabled(false);
    m_pCurrentToleranceMinusEdit->setEnabled(false);
  }
}

void PxiDeviceDataRowPS::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  PxiDeviceDataRow::storeDataControlContents();

  m_pActiveSection->dataBlock()->insert(JSON_VOLTAGE,QJsonValue(m_pVoltageEdit->value()));
  m_pActiveSection->dataBlock()->insert(JSON_VOLTAGE_TOLERANCE_PLUS,QJsonValue(m_pVoltageTolerancePlusEdit->value()));
  m_pActiveSection->dataBlock()->insert(JSON_VOLTAGE_TOLERANCE_MINUS,QJsonValue(m_pVoltageToleranceMinusEdit->value()));

  m_pActiveSection->dataBlock()->insert(JSON_CURRENT,QJsonValue(m_pCurrentEdit->value()));
  m_pActiveSection->dataBlock()->insert(JSON_CURRENT_TOLERANCE_PLUS,QJsonValue(m_pCurrentTolerancePlusEdit->value()));
  m_pActiveSection->dataBlock()->insert(JSON_CURRENT_TOLERANCE_MINUS,QJsonValue(m_pCurrentToleranceMinusEdit->value()));
}

void PxiDeviceDataRowPS::fillDataControlContents()
{
  m_bNoSave = true;

  PxiDeviceDataRow::fillDataControlContents();

  if ( m_pActiveSection )
  {
    m_pVoltageEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_VOLTAGE).toDouble());
    m_pVoltageTolerancePlusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_VOLTAGE_TOLERANCE_PLUS).toDouble());
    m_pVoltageToleranceMinusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_VOLTAGE_TOLERANCE_MINUS).toDouble());

    m_pCurrentEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_CURRENT).toDouble());
    m_pCurrentTolerancePlusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_CURRENT_TOLERANCE_PLUS).toDouble());
    m_pCurrentToleranceMinusEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_CURRENT_TOLERANCE_MINUS).toDouble());
  }

  m_bNoSave = false;
}

/*void PxiDeviceDataRowPS::setVoltageToleranceLimitsSlot(double voltageValue)
{
  static double previousValue;
  m_pVoltageTolerancePlusEdit->setMinimum(voltageValue);
  m_pVoltageToleranceMinusEdit->setMaximum(voltageValue);
  if ( voltageValue<previousValue )
    m_pVoltageTolerancePlusEdit->setValue(voltageValue);

  previousValue = voltageValue;
}*/

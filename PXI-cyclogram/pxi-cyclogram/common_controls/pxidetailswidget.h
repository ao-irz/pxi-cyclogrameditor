#ifndef PXIDETAILSWIDGET_H
#define PXIDETAILSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFrame>
#include <QCheckBox>

#include "pxiswitcher.h"

class PxiDeviceDataRow;

// -----------------------------------------------------------------------
class PxiDetailsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PxiDetailsWidget(const QString& strDeviceName,
                              QWidget *parent, bool withClearButton = true);

    QGridLayout* detailsLayout() const;
    PxiSwitcher* switcher() const;

    QFrame* separatorFrame();
    QVBoxLayout* createLabeledFieldVLayout(QString strLabel,
                                           QWidget* pWidget,
                                           bool addHStretch = true,
                                           bool addVStretch = false);
    QWidget* createLabeledFieldWidget(QString strLabel,
                                           QWidget* pWidget,
                                           bool addStretch = true);

    QVBoxLayout* createCheckableFieldVLayout(QString strLabel,
                                             QWidget* pWidget,
                                             QCheckBox*& pCheckBox,
                                             bool addStretch = true);

  signals:
    void stateChangedSignal(bool bSwitchedOn);
    void addNewDataBlockBeforeSignal();
    void deleteDataBlockSignal();
    void addNewDataBlockAfterSignal();
    void clearCurrentBlockSignal();

  public slots:
    void dataRowBlocksCountChangedSlot(int blocksCount);

private:
    QString m_strDeviceName;
    QHBoxLayout* m_pMainLayout;
    QGridLayout* m_pDetailsLayout;
    QVBoxLayout* m_pButtonLayout;
    PxiSwitcher* m_pDeviceSwitcher;
    QPushButton* m_pAddBlockBeforeButton;
    QPushButton* m_pDeleteBlockButton;
    QPushButton* m_pClearBlockButton;
    QPushButton* m_pAddBlockAfterButton;
    static QPixmap* m_pAddBeforePict;
    static QPixmap* m_pAddAfterPict;
    static QPixmap* m_pMinusPict;
    static QPixmap* m_pClearPict;

    bool m_bWithClearButton;
};

#endif // PXIDETAILSWIDGET_H

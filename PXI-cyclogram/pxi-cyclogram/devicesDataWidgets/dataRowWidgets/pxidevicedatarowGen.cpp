#include "pxidevicedatarowGen.h"
#include "../../global_definitions.h"
#include <math.h>
#include <QMetaEnum>

PxiDeviceDataRowGen::PxiDeviceDataRowGen(const int nDeviceIndex,
                                       const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget *parent):
    PxiDeviceDataRow(GlobalDefinitions::ItemType::ITEM_GENERATOR,
                     nDeviceIndex,
                     strLabelText,
                     normalIcon,
                     selectedIcon,
                     parent)
{
    setupDurationArea();
    setupDetailsArea();
}

double PxiDeviceDataRowGen::amplitudeValue() const
{
  return m_pAmplitudeEdit->value();
}

double PxiDeviceDataRowGen::offsetValue() const
{
  return m_pOffsetEdit->value();
}

double PxiDeviceDataRowGen::frequency() const
{
  return m_nFrequency;
}

GlobalDefinitions::GenFunction PxiDeviceDataRowGen::functionType() const
{
  return m_enCurrentFunction;
}

int PxiDeviceDataRowGen::dutyCycle() const
{
  return m_pDutyCycleEdit->value();
}

void PxiDeviceDataRowGen::clearDetailsWidget()
{
  m_bNoSave = true;

  PxiDeviceDataRow::clearDetailsWidget();

  m_pFunctionBox->setCurrentIndex(0);
  m_pDutyCycleEdit->setValue(50);
  m_pFrequencyEdit->setValue(0);
  m_pFrequencyDimensionBox->setCurrentIndex(0);

  m_pAmplitudeEdit->setValue(0);
  m_pOffsetEdit->setValue(0);
  m_pImpedanceCheck->setChecked(false);

  m_bNoSave = false;
}

QVBoxLayout* PxiDeviceDataRowGen::createFrequencyLayout()
{
  QVBoxLayout* retVal = new QVBoxLayout();
  retVal->setMargin(0);
  retVal->setSpacing(0);

  QHBoxLayout* pHorizLayout = new QHBoxLayout();

  m_pFrequencyLabel = new QLabel("Частота, Гц",m_pDetailsWidget);
  m_pFrequencyLabel->setContentsMargins(0,0,5,0);
  pHorizLayout->addWidget(m_pFrequencyLabel);

  m_pFrequencyDimensionBox = new QComboBox(m_pDetailsWidget);
  m_pFrequencyDimensionBox->setProperty( JSON_FIELD_NAME ,JSON_FREQUENCY);
  QFont f;
  f.setPointSize(9);
  m_pFrequencyDimensionBox->setFont(f);
  m_pFrequencyDimensionBox->addItem("Гц",1);
  m_pFrequencyDimensionBox->addItem("кГц",1000);
  m_pFrequencyDimensionBox->addItem("МГц",1000000);
  m_pFrequencyDimensionBox->setFixedWidth(22);
  m_pFrequencyDimensionBox->setFixedHeight(16);
  pHorizLayout->addWidget(m_pFrequencyDimensionBox);
  pHorizLayout->addStretch(1);
  connect(m_pFrequencyDimensionBox,&QComboBox::currentTextChanged,
          this,&PxiDeviceDataRowGen::frequencyChangedSlot);

  retVal->addLayout(pHorizLayout);

  pHorizLayout = new QHBoxLayout();

  m_pFrequencyEdit = new QDoubleSpinBox(m_pDetailsWidget);
  m_pFrequencyEdit->setProperty( JSON_FIELD_NAME ,JSON_FREQUENCY);
  m_pFrequencyEdit->setMaximum(20000000);
  m_pFrequencyEdit->setDecimals(3);
  pHorizLayout->addWidget(m_pFrequencyEdit);
  pHorizLayout->addStretch(1);
  connect(m_pFrequencyEdit,QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,&PxiDeviceDataRowGen::frequencyChangedSlot);

  retVal->addLayout(pHorizLayout);

  return retVal;
}

void PxiDeviceDataRowGen::setupDetailsArea()
{
  PxiDeviceDataRow::setupDetailsArea();

  QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();

  int setupControlColumn = m_setupControlColumn;

  m_pFunctionBox = new QComboBox(m_pDetailsWidget);
  m_pFunctionBox->setProperty( JSON_FIELD_NAME ,JSON_FUNCTION_TYPE);
  m_pFunctionBox->setIconSize(QSize(64,18));
  m_pFunctionBox->addItem(*(new QIcon(":sine_mid.png")),"",GlobalDefinitions::GenFunction::SINUS);
  m_pFunctionBox->addItem(*(new QIcon(":square_mid.png")),"",GlobalDefinitions::GenFunction::SQUARE);
  m_pFunctionBox->addItem(*(new QIcon(":triangle_mid.png")),"",GlobalDefinitions::GenFunction::TRIANGLE);
  m_pFunctionBox->addItem(*(new QIcon(":ramp_mid.png")),"",GlobalDefinitions::GenFunction::RAMP);
  connect(m_pFunctionBox,&QComboBox::currentTextChanged,
          this,&PxiDeviceDataRowGen::functionChangedSlot);
  pLayout->addLayout( m_pDetailsWidget->createLabeledFieldVLayout("Тип сигнала",m_pFunctionBox),0,m_setupControlColumn++);

  m_pDutyCycleEdit = new QSpinBox(m_pDetailsWidget);
  m_pDutyCycleEdit->setProperty( JSON_FIELD_NAME ,JSON_DUTYCYCLE);
  m_pDutyCycleEdit->setRange(0,100);
  m_pDutyCycleEdit->setValue(50);
  m_pDutyCycleEdit->setSuffix("%");
  m_pDutyCycleWidget = m_pDetailsWidget->createLabeledFieldWidget("Рабочий цикл",m_pDutyCycleEdit);
  pLayout->addWidget(m_pDutyCycleWidget,0,m_setupControlColumn++);
  connect(m_pDutyCycleEdit,QOverload<int>::of(&QSpinBox::valueChanged),
          this,&PxiDeviceDataRowGen::dataControlValueChangedSlot);

  pLayout->addLayout(createFrequencyLayout(),0,m_setupControlColumn);

  m_setupControlColumn = setupControlColumn;

  m_pAmplitudeEdit = new QDoubleSpinBox(m_pDetailsWidget);
  m_pAmplitudeEdit->setProperty( JSON_FIELD_NAME ,JSON_AMPLITUDE);
  m_pAmplitudeEdit->setDecimals(3);
  connect(m_pAmplitudeEdit, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,&PxiDeviceDataRowGen::amplitudeValueChangedSlot);
  pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Амплитуда, Vpp",m_pAmplitudeEdit),1,m_setupControlColumn++);

  m_pOffsetEdit = new QDoubleSpinBox(m_pDetailsWidget);
  m_pOffsetEdit->setProperty( JSON_FIELD_NAME ,JSON_OFFSET);
  m_pOffsetEdit->setDecimals(3);
  connect(m_pOffsetEdit, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          this,&PxiDeviceDataRowGen::offsetValueChangedSlot);
  pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Смещение, V",m_pOffsetEdit),1,m_setupControlColumn++);

  QVBoxLayout* pImpedanceLayout = new QVBoxLayout();
  pImpedanceLayout->setMargin(0);
  pImpedanceLayout->setSpacing(0);
  QLabel* l = new QLabel("Нагрузка 50 Ом",m_pDetailsWidget);
  l->setContentsMargins(0,1,0,0);
  pImpedanceLayout->addWidget(l,0,Qt::AlignTop);
  m_pImpedanceCheck = new QCheckBox(m_pDetailsWidget);
  m_pImpedanceCheck->setProperty( JSON_FIELD_NAME ,JSON_IMPEDANCE);
  m_pImpedanceCheck->setChecked(false);
  connect(m_pImpedanceCheck, &QCheckBox::stateChanged,
          this,&PxiDeviceDataRowGen::impedanceStateChangedSlot);
  pImpedanceLayout->addWidget(m_pImpedanceCheck,0,Qt::AlignVCenter);
  pLayout->addLayout(pImpedanceLayout,1,m_setupControlColumn++);

  functionChangedSlot();
  impedanceStateChangedSlot(m_pImpedanceCheck->checkState());
}

void PxiDeviceDataRowGen::updateDetailsArea()
{
  PxiDeviceDataRow::updateDetailsArea();

  if ( m_pActiveSection )
  {
    m_pFunctionBox->setEnabled(true);
    functionChangedSlot();
    m_pDutyCycleWidget->setEnabled(m_enCurrentFunction==GlobalDefinitions::GenFunction::RAMP ||
                             m_enCurrentFunction==GlobalDefinitions::GenFunction::SQUARE);
    m_pFrequencyEdit->setEnabled(true);
    m_pFrequencyDimensionBox->setEnabled(true);
    frequencyChangedSlot();
    m_pAmplitudeEdit->setEnabled(true);
    m_pOffsetEdit->setEnabled(true);
    m_pImpedanceCheck->setEnabled(true);

    fillDataControlContents();
  }
  else
  {
    m_pFunctionBox->setEnabled(false);
    m_pDutyCycleWidget->setEnabled(false);
    m_pFrequencyEdit->setEnabled(false);
    m_pFrequencyDimensionBox->setEnabled(false);
    m_pAmplitudeEdit->setEnabled(false);
    m_pOffsetEdit->setEnabled(false);
    m_pImpedanceCheck->setEnabled(false);
  }
}

void PxiDeviceDataRowGen::functionChangedSlot()
{
  m_enCurrentFunction = static_cast<GlobalDefinitions::GenFunction>(m_pFunctionBox->currentData().toUInt());

  m_pDutyCycleWidget->setEnabled(m_enCurrentFunction==GlobalDefinitions::GenFunction::RAMP ||
                           m_enCurrentFunction==GlobalDefinitions::GenFunction::SQUARE);

  switch(m_enCurrentFunction)
  {
    case GlobalDefinitions::GenFunction::SINUS:
    {
      m_nMaxFrequency = 20000000;
      break;
    }
    case GlobalDefinitions::GenFunction::SQUARE:
    {
      m_nMaxFrequency = 10000000;
      break;
    }
    default:
    {
      m_nMaxFrequency = 1000000;
    }
  }

  frequencyChangedSlot();
}

void PxiDeviceDataRowGen::frequencyChangedSlot()
{
  m_pFrequencyLabel->setText("Частота, "+m_pFrequencyDimensionBox->currentText());

  m_nFrequencyMultiplier = m_pFrequencyDimensionBox->currentData().toUInt();
  m_pFrequencyEdit->setMaximum(m_nMaxFrequency/m_nFrequencyMultiplier);
  if ( m_pFrequencyEdit->value()*m_nFrequencyMultiplier > m_nMaxFrequency )
  {
    switch (m_nFrequencyMultiplier)
    {
      case 1:
      {
        m_pFrequencyEdit->setValue(m_nMaxFrequency);
        break;
      }
      case 1000:
      {
        m_pFrequencyEdit->setValue(m_nMaxFrequency/1000);
        break;
      }
      case 1000000:
      {
        m_pFrequencyEdit->setValue(m_nMaxFrequency/1000000);
        break;
      }
      default: ;
    }
  }

  m_nFrequency = m_pFrequencyEdit->value()*m_nFrequencyMultiplier;

  dataControlValueChangedSlot();
}

void PxiDeviceDataRowGen::impedanceStateChangedSlot(int state)
{
  Qt::CheckState checkState = static_cast<Qt::CheckState>(state);

  switch ( checkState )
  {
    case Qt::CheckState::Checked:
    {
      nMaxAmplitude = 12;
      break;
    }
    default:
    {
      nMaxAmplitude = 24;
    }
  }
  m_pAmplitudeEdit->setMaximum( nMaxAmplitude );

  amplitudeValueChangedSlot( m_pAmplitudeEdit->value() );
  dataControlValueChangedSlot();
}

void PxiDeviceDataRowGen::amplitudeValueChangedSlot(double amplitudeValue)
{
  if ( m_pImpedanceCheck->isChecked() )
    m_pOffsetEdit->setMaximum(0);
  else
    m_pOffsetEdit->setMaximum( amplitudeValue/2 );

  dataControlValueChangedSlot();
}

void PxiDeviceDataRowGen::offsetValueChangedSlot(double newValue)
{
  if ( newValue>m_pAmplitudeEdit->value() )
    m_pOffsetEdit->setValue( m_pAmplitudeEdit->value() );

  dataControlValueChangedSlot();
}

void PxiDeviceDataRowGen::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  PxiDeviceDataRow::storeDataControlContents();

  GlobalDefinitions::GenFunction function = functionType();
  m_pActiveSection->dataBlock()->insert(JSON_FUNCTION_TYPE,
                                        QJsonValue(QVariant::fromValue(function).toString().toLower()));
  if ( function==GlobalDefinitions::GenFunction::SINUS ||
       function==GlobalDefinitions::GenFunction::TRIANGLE )
    m_pActiveSection->dataBlock()->remove(JSON_DUTYCYCLE);
  else
    m_pActiveSection->dataBlock()->insert(JSON_DUTYCYCLE,QJsonValue(dutyCycle()));

  m_pActiveSection->dataBlock()->insert(JSON_FREQUENCY,
                                        QJsonValue(frequency()));

  if ( m_pImpedanceCheck->isChecked() )
    m_pActiveSection->dataBlock()->insert(JSON_IMPEDANCE,QJsonValue(50));
  else
    m_pActiveSection->dataBlock()->remove(JSON_IMPEDANCE);

  m_pActiveSection->dataBlock()->insert(JSON_AMPLITUDE,
                                        QJsonValue(amplitudeValue()));
  m_pActiveSection->dataBlock()->insert(JSON_OFFSET,
                                        QJsonValue(offsetValue()));
}

void PxiDeviceDataRowGen::fillDataControlContents()
{
  m_bNoSave = true;

  PxiDeviceDataRow::fillDataControlContents();

  if ( m_pActiveSection )
  {
    auto &&functionEnum = QMetaEnum::fromType<GlobalDefinitions::GenFunction>();
    int storedFunction = functionEnum.keyToValue(m_pActiveSection->dataBlock()->value(JSON_FUNCTION_TYPE).toString().toUpper().toLocal8Bit().data());
    for(int i=0; i<m_pFunctionBox->count(); i++)
    {
      if ( m_pFunctionBox->itemData(i).toInt() == storedFunction )
      {
           m_pFunctionBox->setCurrentIndex(i);
           break;
      }
    }
    if ( storedFunction==GlobalDefinitions::GenFunction::SINUS ||
         storedFunction==GlobalDefinitions::GenFunction::TRIANGLE )
    {
      m_pDutyCycleWidget->setEnabled(false);
      m_pDutyCycleEdit->setValue(50);
    }
    else
    {
      m_pDutyCycleWidget->setEnabled(true);
      m_pDutyCycleEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_DUTYCYCLE).toInt());
    }

    double freq = m_pActiveSection->dataBlock()->value(JSON_FREQUENCY).toDouble();
    if ( freq<1000 )
      m_pFrequencyDimensionBox->setCurrentIndex(0);
    else
    if ( freq>=1000 and freq<1000000 )
    {
      freq /=1000;
      m_pFrequencyDimensionBox->setCurrentIndex(1);
    }
    else
    if ( freq>=1000000 )
    {
      freq /= 1000000;
      m_pFrequencyDimensionBox->setCurrentIndex(2);
    }
    m_pFrequencyEdit->setValue(freq);

    m_pAmplitudeEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_AMPLITUDE).toDouble());
    m_pOffsetEdit->setValue(m_pActiveSection->dataBlock()->value(JSON_OFFSET).toDouble());
    m_pImpedanceCheck->setChecked(!(m_pActiveSection->dataBlock()->value(JSON_IMPEDANCE) == QJsonValue::Undefined));

  }

  m_bNoSave = false;
}


#include "thermodurationbar.h"
#include "../thermoschedulesection.h"
#include <cmath>
#include <QApplication>

ThermoDurationBar::ThermoDurationBar(QWidget* parent,ThermoScheduleSection* pParentSection) : QWidget(parent)
  ,m_pParent(parent)
  ,m_pParentSection(pParentSection)
  ,m_bCanHorResize(false)
  ,m_bCanVerResize(false)
  ,m_bCanAcceptWheel(false)
  ,m_bHorResizing(false)
  ,m_bLeftSideResizing(false)
  ,m_bRightSideResizing(false)
  ,m_bVerResizing(false)
  ,m_bSelected(false)
  ,m_bOnState(false)
{

  m_cResizeHorCursor = QCursor(Qt::SizeHorCursor);
  m_cResizeVerCursor = QCursor(Qt::SizeVerCursor);
  m_defaultCursor = QCursor(Qt::ArrowCursor);
  m_customDefaultCursor = QCursor(QPixmap(":mouse_pointer.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation));

  setMouseTracking(true);

  m_heatingUnselectedBrush = QBrush(QPixmap(":/images/dense5patternRed.png"));
  m_maintainingUnselectedBrush = QBrush(QPixmap(":/images/dense5patternGreen.png"));
  m_coolingUnselectedBrush = QBrush(QPixmap(":/images/dense5patternBlue.png"));
  m_heatingSelectedBrush = QBrush(QColor(255,102,51,150));
  m_maintainingSelectedBrush = QBrush(QColor(0,200,0,150));
  m_coolingSelectedBrush = QBrush(QColor(64,64,240,150));
  m_offSelectedBrush = QBrush(QColor(64,64,64,150),Qt::Dense2Pattern);
  m_offUnselectedBrush = QBrush(QColor(64,64,64,150),Qt::DiagCrossPattern);

  m_penSelected = QPen(QColor(153, 102, 51,180));
  m_penNormal =  QPen(QColor(0,0,0,100));
  m_penSelected.setWidth(2);

  m_pParentSection->addBar(this);
  connect(m_pParentSection,&ThermoScheduleSection::thermoModeChangedSignal,
          this,&ThermoDurationBar::thermoModeChangedSlot);

  m_nThermoMode = m_pParentSection->thermoMode();

}

void ThermoDurationBar::select()
{
  m_bSelected = true;

  repaint();
}

void ThermoDurationBar::unselect()
{
  m_bSelected = false;

  repaint();
}

void ThermoDurationBar::setEnable(const bool bEnabled)
{
  m_bOnState = bEnabled;

  /*if ( m_bSelected )
    select();
  else
    unselect();*/
  repaint();
}

void ThermoDurationBar::thermoModeChangedSlot(GlobalDefinitions::ThermoMode thermoMode)
{
  m_nThermoMode = thermoMode;

  repaint();
}

bool ThermoDurationBar::isHorResizing(QMouseEvent *e)
{
  bool retVal = true;

  if ( !m_bHorResizing )
  {
    int nFinalTemperatureHeightLimit = m_nFinalTemperatureBarHeight+
                                       (int)roundf((float)(m_nWidgetHeight-m_nFinalTemperatureBarHeight)*0.2f);
    int nInitialTemperatureHeightLimit = m_nInitialTemperatureBarHeight+
                                       (int)roundf((float)(m_nWidgetHeight-m_nInitialTemperatureBarHeight)*0.2f);
    m_bRightSideResizing = e->x()>= width()-5 && e->x()< width() &&
                           e->y()>nFinalTemperatureHeightLimit && e->y()<m_nWidgetHeight &&
                           !m_pParentSection->isLastSection();
    m_bLeftSideResizing = e->x()> 0 && e->x()<= 5 &&
                          e->y()>nInitialTemperatureHeightLimit && e->y()<m_nWidgetHeight &&
                          !m_pParentSection->isFirstSection();
    if ( m_bRightSideResizing || m_bLeftSideResizing )
    {
      setCursor(m_cResizeHorCursor);
      m_bCanHorResize = true;
    }
    else
    {
      m_bCanHorResize = false;
      retVal = false;
    }
  }
  else
  {
    if ( m_bRightSideResizing && e->x()>DURATION_BLOCKS_SPACING )
    {
      float fKoeff = static_cast<float>((e->x()-(width()-e->x())))/m_nPreviousWidth;
      emit resizingDurationSignal( fKoeff );
    }
    if ( m_bLeftSideResizing && e->x()<(width()-DURATION_BLOCKS_SPACING) )
    {
      float fKoeff = static_cast<float>(width()-e->x()-e->x())/m_nPreviousWidth;
      emit resizingDurationSignal( fKoeff );
    }
  }

  return retVal;
}

bool ThermoDurationBar::isVerResizing(QMouseEvent *e)
{
  bool retVal = true;

  if ( !m_bVerResizing )
  {
    if ( m_pSubPolygon.containsPoint(e->pos(),Qt::OddEvenFill) )
    {
      setCursor(m_cResizeVerCursor);
      m_bCanVerResize = true;
    }
    else
    {
      m_bCanVerResize = false;
      retVal = false;
    }
  }
  else
  {
    if ( e->y()<(THERMO_FRAME_GRID_HEIGHT-DURATION_BLOCKS_SPACING) )
      emit changingTemperatureSignal( m_nFinalTemperatureBarHeight-e->y() );
  }

  return retVal;
}

void ThermoDurationBar::mouseMoveEvent(QMouseEvent *e)
{
  m_bCanAcceptWheel = true;

  if ( !isHorResizing(e) )
  if ( !isVerResizing(e) )
  {
    if ( m_pBarPolygon.containsPoint(e->pos(),Qt::OddEvenFill) )
      setCursor(m_customDefaultCursor);
    else
      setCursor(m_defaultCursor);
  }
}

void ThermoDurationBar::mousePressEvent(QMouseEvent*)
{
  if ( !m_bSelected )
    m_pParentSection->select();

  if ( m_bCanHorResize )
  {
    m_bHorResizing = true;
    emit startResizeDurationSignal();
    m_nPreviousWidth = width();
  }

  if ( m_bCanVerResize )
    m_bVerResizing = true;
}

void ThermoDurationBar::mouseReleaseEvent(QMouseEvent*)
{
  if ( m_bHorResizing )
  {
    emit stopResizeDurationSignal();
    m_bHorResizing = false;
    m_bRightSideResizing = false;
    m_bLeftSideResizing = false;
  }

  if ( m_bVerResizing )
    m_bVerResizing = false;
}

void ThermoDurationBar::wheelEvent(QWheelEvent *e)
{
  if ( !m_pBarPolygon.containsPoint(QPoint(e->x(), e->y()),Qt::OddEvenFill) )
    return;

  m_nPreviousWidth = width();

  QPoint numPixels = e->angleDelta();
  if ( m_bCanAcceptWheel && !numPixels.isNull() )
  {
    if ( !m_bSelected )
      m_pParentSection->select();

    int delta = numPixels.ry() > 0 ? DURATION_BLOCKS_RESIZE_STEP :
                numPixels.ry() < 0 ? DURATION_BLOCKS_RESIZE_STEP*-1 : 0 ;

    Qt::KeyboardModifiers keys = QApplication::keyboardModifiers();
    if ( keys&Qt::ShiftModifier) // изменение температуры
    {
      if ( m_nFinalTemperatureBarHeight<(THERMO_FRAME_GRID_HEIGHT-DURATION_BLOCKS_SPACING) )
        emit changingTemperatureSignal( delta );
    }
    else
    {
      if ( width()>DURATION_BLOCKS_SPACING ) // изменение длительности
      {
        emit startResizeDurationSignal();
        emit resizingDurationSignal( static_cast<float>(width()+delta)/m_nPreviousWidth );
        emit stopResizeDurationSignal();
      }
    }

    e->accept();
  }
  else
    e->ignore();
}

void ThermoDurationBar::leaveEvent(QEvent* e)
{
  Q_UNUSED(e)

  m_bCanAcceptWheel = false;
  setCursor(m_defaultCursor);
}

void ThermoDurationBar::paintEvent(QPaintEvent *event)
{
  Q_UNUSED(event)

  QPainter painter(this);

  m_nWidgetWidth = this->width()-1;
  m_nWidgetHeight = this->height()-1;
  m_nInitialTemperatureBarHeight = static_cast<int>(roundf((static_cast<float>(THERMO_FRAME_GRID_LINE_STEP)/10)*(static_cast<float>(OVEN_MAX_TEMPERATURE)-m_pParentSection->initialTemperature())));
  m_nFinalTemperatureBarHeight = static_cast<int>(roundf((static_cast<float>(THERMO_FRAME_GRID_LINE_STEP)/10)*(static_cast<float>(OVEN_MAX_TEMPERATURE)-m_pParentSection->finalTemperature())));

  painter.setPen(m_bSelected ? m_penSelected : m_penNormal);
  if ( !m_bOnState )
    painter.setBrush(m_bSelected ? m_offSelectedBrush : m_offUnselectedBrush);
  else
  switch(m_nThermoMode)
  {
    case GlobalDefinitions::ThermoMode::HEATING:
    {
      painter.setBrush(m_bSelected ? m_heatingSelectedBrush : m_heatingUnselectedBrush);
      break;
    }
    case GlobalDefinitions::ThermoMode::MAINTAINING:
    {
      painter.setBrush(m_bSelected ? m_maintainingSelectedBrush : m_maintainingUnselectedBrush);
      break;
    }
    case GlobalDefinitions::ThermoMode::COOLING:
    {
      painter.setBrush(m_bSelected ? m_coolingSelectedBrush : m_coolingUnselectedBrush );
      break;
    }
  }

  m_pBarPolygon.clear();
  m_pBarPolygon << QPoint(0, m_nInitialTemperatureBarHeight) << QPoint(m_nWidgetWidth, m_nFinalTemperatureBarHeight) <<
             QPoint(m_nWidgetWidth, m_nWidgetHeight) << QPoint(0, m_nWidgetHeight);
  painter.drawPolygon(m_pBarPolygon);

  m_pSubPolygon.clear();
  int heightDiference = (m_nFinalTemperatureBarHeight-m_nInitialTemperatureBarHeight)/2;
  m_pSubPolygon << QPoint(m_nWidgetWidth/2,m_nInitialTemperatureBarHeight+heightDiference)<<QPoint(m_nWidgetWidth, m_nFinalTemperatureBarHeight) <<
                   QPoint(m_nWidgetWidth, m_nFinalTemperatureBarHeight+10) << QPoint(m_nWidgetWidth/2,m_nInitialTemperatureBarHeight+heightDiference+10);
}

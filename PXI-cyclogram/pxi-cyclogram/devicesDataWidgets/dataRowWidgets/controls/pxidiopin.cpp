#include "pxidiopin.h"

QStringList PxiDioPin::pinModeValues = {"none","read","write"};
QStringList PxiDioPin::pinModeTexts = {"нет","чтение","запись"};

PxiDioPin::PxiDioPin(int nPinNumber, QWidget *parent) : QGroupBox(parent)
  ,m_pParent(parent)
  ,m_nPinNumber(nPinNumber)
{
  setCheckable(true);
  setFlat(true);
  setChecked(false);
  setTitle(QString::number(m_nPinNumber+1));
  setFixedSize(QSize(165,64));

  QFont f;
  f.setPointSize(10);

  QLabel* modeLabel = new QLabel("режим", this);
  modeLabel->setFont(f);
  modeLabel->setGeometry(QRect(5, 20, 63, 17));

  m_pModeBox = new QComboBox(this);
  m_pModeBox->setFont(f);
  m_pModeBox->addItems(pinModeTexts);
  m_pModeBox->setGeometry(QRect(5, 37, 80, 20));
  connect(m_pModeBox,QOverload<int>::of(&QComboBox::currentIndexChanged),
          this,&PxiDioPin::valueChanged);
  m_pModeBox->setCurrentIndex(0);

  QLabel* valueLabel = new QLabel("низк-высок", this);
  valueLabel->setFont(f);
  valueLabel->setGeometry(QRect(90, 20, 74, 17));

  m_pValueSwitcher = new PxiSwitcher(this);
  m_pValueSwitcher->setGeometry(QRect(110, 40, 24, 16));
  connect(m_pValueSwitcher,&PxiSwitcher::valueChanged,this,&PxiDioPin::valueChanged);

  connect(this,&QGroupBox::toggled,this,&PxiDioPin::toggledSlot);
}

int PxiDioPin::pinNumber() const
{
  return m_nPinNumber;
}

QString PxiDioPin::pinMode() const
{
  return pinModeValues[m_pModeBox->currentIndex()];
}

bool PxiDioPin::pinValue() const
{
  return m_pValueSwitcher->valueBool();
}

void PxiDioPin::setMode(const QString &strMode)
{
  m_pModeBox->setCurrentIndex(pinModeValues.indexOf(strMode));
}

void PxiDioPin::setValue(const bool &bValue)
{
  m_pValueSwitcher->setState( bValue );
}

void PxiDioPin::toggledSlot(bool onState)
{
  if( !onState )
  {
    m_pModeBox->setCurrentIndex(0);
    m_pValueSwitcher->setState(false);
  }

  emit valueChanged();
}

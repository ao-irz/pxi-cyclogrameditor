#ifndef PXICLICKABLELABEL_H
#define PXICLICKABLELABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QPixmap>

class PxiClickableLabel : public QLabel
{
    Q_OBJECT
public:
    explicit PxiClickableLabel(const QString strLabelText,
                               QWidget* parent=nullptr);

    ~PxiClickableLabel();

    void setIcons(const QPixmap& normalIcon, const QPixmap& selectedIcon );
    void select();
    void unselect();

signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);

private:
    bool m_bIconLabel;
    QPixmap m_NormalIcon;
    QPixmap m_SelectedIcon;
    QFont m_selectedFont;
    QFont m_normalFont;

};

#endif // PXICLICKABLELABEL_H

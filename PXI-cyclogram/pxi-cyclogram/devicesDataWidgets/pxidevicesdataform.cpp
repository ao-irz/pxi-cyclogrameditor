#include "pxidevicesdataform.h"
#include "../mainwindow.h"
#include <QFileDialog>

#define ICON_SIZE 32

PxiDevicesDataForm::PxiDevicesDataForm(QWidget *parent) : DataForm(parent)
,m_nSelectedItemType(GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY)
,m_nSelectedItemIndex(0)
{
  m_nDataFormType = GlobalDefinitions::DataFormPageType::PXI;

  m_pPsIcon = new QIcon(POWER_SUPPLY_ICON);
  m_pGenIcon = new QIcon(GENERATOR_ICON);
  m_pTCIcon = new QIcon(THERMOCOUPLE_ICON);
  m_pDioIcon = new QIcon(DIO_ICON);
  m_pPsSelectedIcon = new QIcon(POWER_SUPPLY_SELECTED_ICON);
  m_pGenSelectedIcon = new QIcon(GENERATOR_SELECTED_ICON);
  m_pTCSelectedIcon = new QIcon(THERMOCOUPLE_SELECTED_ICON);
  m_pDioSelectedIcon = new QIcon(DIO_SELECTED_ICON);

  setupForm();
}

void PxiDevicesDataForm::setupForm()
{
  DataForm::setupForm();

  const int deviceLabelColumn = 0;
  const int vSeparatorColumn = 1;
  const int durationWidgetColumn = 2;

  PxiDeviceDataRow* pDeviceDataRow;

  // создать строки для источников питания
  for(int j=0; j<POWER_SUPPLY_NUMBER; j++)
  {
    pDeviceDataRow = new PxiDeviceDataRowPS(j, // itemIndex among similar devices
                                            POWER_SUPPLY_NAME+QString::number(j+1), // itemLabel
                                            m_pPsIcon->pixmap(ICON_SIZE), // normalIcon
                                            m_pPsSelectedIcon->pixmap(ICON_SIZE),// selectedIcon
                                            this);
    m_mapDeviceDataRows.insert(QPair<GlobalDefinitions::ItemType,int>
                               (GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY,j),pDeviceDataRow);
    m_pDataGridLayout->addLayout(dynamic_cast<QLayout*>(pDeviceDataRow->deviceLabel()),m_nSetupCurrentRow,deviceLabelColumn);
    m_pDataGridLayout->addWidget(pDeviceDataRow->durationWidget(),m_nSetupCurrentRow,durationWidgetColumn);
    m_nSetupCurrentRow++;
  }

  // Создать строку для генератора
  pDeviceDataRow = new PxiDeviceDataRowGen(0,
                                           GENERATOR_NAME,
                                           m_pGenIcon->pixmap(ICON_SIZE),
                                           m_pGenSelectedIcon->pixmap(ICON_SIZE),this);
  m_mapDeviceDataRows.insert(QPair<GlobalDefinitions::ItemType,int>
                             (GlobalDefinitions::ItemType::ITEM_GENERATOR,0),pDeviceDataRow);
  m_pDataGridLayout->addLayout(dynamic_cast<QLayout*>(pDeviceDataRow->deviceLabel()),m_nSetupCurrentRow,deviceLabelColumn);
  m_pDataGridLayout->addWidget(pDeviceDataRow->durationWidget(),m_nSetupCurrentRow,durationWidgetColumn);
  m_nSetupCurrentRow++;

  // Создать строку для DIO
  pDeviceDataRow = new PxiDeviceDataRowDio(0,
                                           DIO_NAME,
                                           m_pDioIcon->pixmap(ICON_SIZE),
                                           m_pDioSelectedIcon->pixmap(ICON_SIZE),this);
  m_mapDeviceDataRows.insert(QPair<GlobalDefinitions::ItemType,int>
                             (GlobalDefinitions::ItemType::ITEM_DIO,0),pDeviceDataRow);
  m_pDataGridLayout->addLayout(dynamic_cast<QLayout*>(pDeviceDataRow->deviceLabel()),m_nSetupCurrentRow,deviceLabelColumn);
  m_pDataGridLayout->addWidget(pDeviceDataRow->durationWidget(),m_nSetupCurrentRow,durationWidgetColumn);
  m_nSetupCurrentRow++;

  // Создать строку для SMU
  pDeviceDataRow = new PxiDeviceDataRowSmu(0, // itemIndex among similar devices
                                           SUO_NAME, // itemLabel
                                           m_pPsIcon->pixmap(ICON_SIZE), // normalIcon
                                           m_pPsSelectedIcon->pixmap(ICON_SIZE),// selectedIcon
                                           this);
  m_mapDeviceDataRows.insert(QPair<GlobalDefinitions::ItemType,int>
                             (GlobalDefinitions::ItemType::ITEM_SMU,0),pDeviceDataRow);
  m_pDataGridLayout->addLayout(dynamic_cast<QLayout*>(pDeviceDataRow->deviceLabel()),m_nSetupCurrentRow,deviceLabelColumn);
  m_pDataGridLayout->addWidget(pDeviceDataRow->durationWidget(),m_nSetupCurrentRow,durationWidgetColumn);
  m_nSetupCurrentRow++;

  // Создать строку для термопары
  pDeviceDataRow = new PxiDeviceDataRowThermoCouple(0,
                                                    THERMOCOUPLE_NAME,
                                                    m_pTCIcon->pixmap(ICON_SIZE),
                                                    m_pTCSelectedIcon->pixmap(ICON_SIZE),this);
  m_mapDeviceDataRows.insert(QPair<GlobalDefinitions::ItemType,int>
                             (GlobalDefinitions::ItemType::ITEM_THERMOCOUPLE,0),pDeviceDataRow);
  m_pDataGridLayout->addLayout(dynamic_cast<QLayout*>(pDeviceDataRow->deviceLabel()),m_nSetupCurrentRow,deviceLabelColumn);
  m_pDataGridLayout->addWidget(pDeviceDataRow->durationWidget(),m_nSetupCurrentRow,durationWidgetColumn);
  m_nSetupCurrentRow++;

  QFrame* vSeparator = new QFrame(this);
  vSeparator->setFrameShape(QFrame::VLine);
  vSeparator->setFrameShadow(QFrame::Sunken);
  m_pDataGridLayout->addWidget(vSeparator,2,vSeparatorColumn,m_nSetupCurrentRow-2,1,Qt::AlignLeft);

  m_pHSeparator = new QFrame(this);
  m_pHSeparator->setFrameShape(QFrame::HLine);
  m_pHSeparator->setFrameShadow(QFrame::Sunken);
  m_pDataGridLayout->addWidget(m_pHSeparator,m_nSetupCurrentRow,0,1,3);
  m_nSetupCurrentRow++;

  m_pDeviceParametersLayout = new QStackedLayout();
  m_pDataGridLayout->addLayout(m_pDeviceParametersLayout,m_nSetupCurrentRow,0,1,3);
  m_nSetupCurrentRow++;

  connect(this,&PxiDevicesDataForm::activateDataPage,
          m_pDeviceParametersLayout,&QStackedLayout::setCurrentWidget);

  for( auto pDeviceDataRowInMap : m_mapDeviceDataRows )
  {
    pDeviceDataRowInMap->finalizeDetailsWidget();
    m_pDeviceParametersLayout->addWidget(dynamic_cast<QWidget*>(pDeviceDataRowInMap->detailsWidget()));

    connect(pDeviceDataRowInMap,&PxiDeviceDataRow::deviceSelectedSignal,
            this,&PxiDevicesDataForm::itemLabelClickedSlot);
    connect(this,&DataForm::resetDataSignal,
            pDeviceDataRowInMap,&PxiDeviceDataRow::resetDataSlot);
    connect(pDeviceDataRowInMap,&PxiDeviceDataRow::showStatusBarMsgSignal,
            this,&DataForm::showStatusBarMsgSlot);
  }

  m_pDataGridLayout->setHorizontalSpacing(2);
  m_pDataGridLayout->setColumnStretch(0,0);
  m_pDataGridLayout->setColumnStretch(1,0);
  m_pDataGridLayout->setColumnStretch(2,0);
  //m_pDataGridLayout->setColumnStretch(3,1);

  m_mapDeviceDataRows.value(QPair<GlobalDefinitions::ItemType,int>(m_nSelectedItemType,m_nSelectedItemIndex))->select();
}

void PxiDevicesDataForm::itemLabelClickedSlot(GlobalDefinitions::ItemType nItemType, int nItemIndex)
{
  if ( nItemType!=m_nSelectedItemType || nItemIndex!= m_nSelectedItemIndex )
  {
    m_mapDeviceDataRows.value(QPair<GlobalDefinitions::ItemType,int>(m_nSelectedItemType,m_nSelectedItemIndex))->unselect();
    m_nSelectedItemType = nItemType;
    m_nSelectedItemIndex = nItemIndex;

    QWidget* pDataPage = dynamic_cast<QWidget*>(m_mapDeviceDataRows.value(QPair<GlobalDefinitions::ItemType,int>(m_nSelectedItemType,m_nSelectedItemIndex))->detailsWidget());
    if ( pDataPage )
      emit activateDataPage( pDataPage );
  }
}

bool PxiDevicesDataForm::checkJson( QString& errMsg )
{
  bool retVal = true;

  QJsonObject rootObject = m_jsonDataLoaded.object();
  if ( !rootObject.contains(JSON_KEY_CHIPSNAME) ||
       !rootObject.contains(JSON_KEY_CHIPSBATCH)
     )
  {
    errMsg = "Возможно файл поврежден, или не содержит данных циклограммы для работы PXI-оборудования!";
    retVal = false;
  }

  return retVal;
}

bool PxiDevicesDataForm::canClose(QString msgTitle)
{
  Q_UNUSED(msgTitle)

  return DataForm::canClose("Циклограмма PXI-устройств");
}

QMessageBox::StandardButton PxiDevicesDataForm::showConfirmation(const QString& title, const QString& msgText)
{
  QMessageBox* pMessageBox;
  QMessageBox::StandardButton btnAnsver = QMessageBox::No;

  pMessageBox = new QMessageBox(QMessageBox::Question,title,
                                msgText,
                                QMessageBox::Yes|QMessageBox::No,m_pParent);
  pMessageBox->setWindowModality(Qt::ApplicationModal);
  btnAnsver = static_cast<QMessageBox::StandardButton>(pMessageBox->exec());

  delete pMessageBox;

  return btnAnsver;
}

void PxiDevicesDataForm::collectData()
{
  m_jsonDataEditing = QJsonDocument();

  QJsonObject rootObject = QJsonObject();

  QJsonArray powerSupplyArray;
  QJsonArray generatorArray;
  QJsonArray thermoCoupleArray;
  QJsonArray dioArray;
  QJsonArray smuArray;
  bool lastStateSwitchedOn;

  for(auto mapDeviceKey: m_mapDeviceDataRows.keys())
  {
    if (m_mapDeviceDataRows.value(mapDeviceKey)->dataRowBlocksCount()==0)
      continue;

    switch(mapDeviceKey.first)
    {
      case GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY:
      {
        QJsonObject psObject;
        QJsonArray psSheduleArray;
        lastStateSwitchedOn = false;
        for(auto& section : m_mapDeviceDataRows.value(mapDeviceKey)->sectionList())
        {
          if (section->dataBlock()->value(JSON_ENABLE).toBool())
            psSheduleArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
          else
          {
            QJsonObject tmpObj;
            tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
            tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
            psSheduleArray.append(tmpObj);
          }
          lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
        }
        if ( lastStateSwitchedOn )
        {
          QJsonObject tmpObj;
          tmpObj[JSON_ENABLE] = false;
          tmpObj[JSON_DURATION] = "0";
          psSheduleArray.append(tmpObj);
        }

        psObject[JSON_KEY_PSNUMBER] = mapDeviceKey.second;
        psObject[JSON_KEY_PSSCHEDULE] = psSheduleArray;

        powerSupplyArray.append(psObject);
        break;
      }
      case GlobalDefinitions::ItemType::ITEM_GENERATOR:
      {
        lastStateSwitchedOn = false;
        for(auto& section : m_mapDeviceDataRows.value(mapDeviceKey)->sectionList())
        {
          if (section->dataBlock()->value(JSON_ENABLE).toBool())
            generatorArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
          else
          {
            QJsonObject tmpObj;
            tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
            tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
            generatorArray.append(tmpObj);
          }
          lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
        }
        if ( lastStateSwitchedOn )
        {
          QJsonObject tmpObj;
          tmpObj[JSON_ENABLE] = false;
          tmpObj[JSON_DURATION] = "0";
          generatorArray.append(tmpObj);
        }

        break;
      }
      case GlobalDefinitions::ItemType::ITEM_THERMOCOUPLE:
      {
        lastStateSwitchedOn = false;
        for(auto& section : m_mapDeviceDataRows.value(mapDeviceKey)->sectionList())
        {
          if (section->dataBlock()->value(JSON_ENABLE).toBool())
            thermoCoupleArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
          else
          {
            QJsonObject tmpObj;
            tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
            tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
            thermoCoupleArray.append(tmpObj);
          }
          lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
        }
        if ( lastStateSwitchedOn )
        {
          QJsonObject tmpObj;
          tmpObj[JSON_ENABLE] = false;
          tmpObj[JSON_DURATION] = "0";
          thermoCoupleArray.append(tmpObj);
        }

        break;
      }
      case GlobalDefinitions::ItemType::ITEM_SMU:
      {
        lastStateSwitchedOn = false;
        for(auto& section : m_mapDeviceDataRows.value(mapDeviceKey)->sectionList())
        {
          if (section->dataBlock()->value(JSON_ENABLE).toBool())
            smuArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
          else
          {
            QJsonObject tmpObj;
            tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
            tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
            smuArray.append(tmpObj);
          }
          lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
        }
        if ( lastStateSwitchedOn )
        {
          QJsonObject tmpObj;
          tmpObj[JSON_ENABLE] = false;
          tmpObj[JSON_DURATION] = "0";
          smuArray.append(tmpObj);
        }

        break;
      }
      case GlobalDefinitions::ItemType::ITEM_DIO:
      {
        lastStateSwitchedOn = false;
        for(auto& section : m_mapDeviceDataRows.value(mapDeviceKey)->sectionList())
        {
          if (section->dataBlock()->value(JSON_ENABLE).toBool())
            dioArray.append(static_cast<QJsonObject>(*(dynamic_cast<ScheduleDataBlock*>(section->dataBlock()))));
          else
          {
            QJsonObject tmpObj;
            tmpObj[JSON_ENABLE] = section->dataBlock()->value(JSON_ENABLE).toBool();
            tmpObj[JSON_DURATION] = section->dataBlock()->value(JSON_DURATION).toString();
            dioArray.append(tmpObj);
          }
          lastStateSwitchedOn = section->dataBlock()->value(JSON_ENABLE).toBool();
        }
        if ( lastStateSwitchedOn )
        {
          QJsonObject tmpObj;
          tmpObj[JSON_ENABLE] = false;
          tmpObj[JSON_DURATION] = "0";
          dioArray.append(tmpObj);
        }

        break;
      }
      default:;
    }
  }

  if ( !powerSupplyArray.isEmpty() )
    rootObject[JSON_KEY_POWERSUPPLIES] = powerSupplyArray;
  if ( !generatorArray.isEmpty() )
    rootObject[JSON_KEY_GENERATOR] = generatorArray;
  if ( !thermoCoupleArray.isEmpty() )
    rootObject[JSON_KEY_THERMOCOUPLE] = thermoCoupleArray;
  if ( !smuArray.isEmpty() )
    rootObject[JSON_KEY_SMU] = smuArray;
  if ( !dioArray.isEmpty() )
    rootObject[JSON_KEY_DIO] = dioArray;

  if ( !rootObject.isEmpty() )
  {
    rootObject[JSON_KEY_CHIPSNAME] = m_pChipsNameEdit->text();
    rootObject[JSON_KEY_CHIPSBATCH] = m_pChipsBatchEdit->text();

    m_jsonDataEditing.setObject(rootObject);
  }


}

void PxiDevicesDataForm::distributeData(QString importFileName)
{
  Q_UNUSED(importFileName)

  QJsonObject rootObject = m_jsonDataLoaded.object();

  m_pChipsNameEdit->setText(rootObject[JSON_KEY_CHIPSNAME].toString());
  m_pChipsBatchEdit->setText(rootObject[JSON_KEY_CHIPSBATCH].toString());

  PxiDeviceDataRow* rowToSelect = nullptr;

  QJsonArray entityArray = rootObject[JSON_KEY_POWERSUPPLIES].toArray();
  if ( entityArray.count()>0 )
  {
    for( auto psArrayElement: entityArray )
    {
      QJsonObject psObject = psArrayElement.toObject();
      int psNumber = psObject[JSON_KEY_PSNUMBER].toInt();
      QJsonArray psScheduleArray = psObject[JSON_KEY_PSSCHEDULE].toArray();

      m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY,psNumber) )->fillWithLoadedData(psScheduleArray);
      if ( !rowToSelect )
        rowToSelect = m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_POWER_SUPPLY,psNumber) );
    }
  }

  entityArray = rootObject[JSON_KEY_GENERATOR].toArray();
  if ( entityArray.count()>0 )
  {
    m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_GENERATOR,0) )->fillWithLoadedData(entityArray);
    if ( !rowToSelect )
      rowToSelect = m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_GENERATOR,0) );
  }

  entityArray = rootObject[JSON_KEY_THERMOCOUPLE].toArray();
  if ( entityArray.count()>0 )
  {
    m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_THERMOCOUPLE,0) )->fillWithLoadedData(entityArray);
    if ( !rowToSelect )
      rowToSelect = m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_THERMOCOUPLE,0) );
  }

  entityArray = rootObject[JSON_KEY_SMU].toArray();
  if ( entityArray.count()>0 )
  {
    m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_SMU,0) )->fillWithLoadedData(entityArray);
    if ( !rowToSelect )
      rowToSelect = m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_SMU,0) );
  }

  entityArray = rootObject[JSON_KEY_DIO].toArray();
  if ( entityArray.count()>0 )
  {
    m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_DIO,0) )->fillWithLoadedData(entityArray);
    if ( !rowToSelect )
      rowToSelect = m_mapDeviceDataRows.value( QPair<GlobalDefinitions::ItemType,int>(GlobalDefinitions::ItemType::ITEM_DIO,0) );
  }

  if ( rowToSelect )
    rowToSelect->sectionList()[0]->select();
}

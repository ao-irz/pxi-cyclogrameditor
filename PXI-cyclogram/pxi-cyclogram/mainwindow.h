#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QToolBar>
#include <QCloseEvent>
#include <QTabWidget>
#include <QStatusBar>

#include "devicesDataWidgets/pxidevicesdataform.h"
#include "devicesDataWidgets/thermodataform.h"

QT_BEGIN_NAMESPACE
namespace Ui { class PxiCMainWindow; }
QT_END_NAMESPACE

class PxiCycloMainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    PxiCycloMainWindow(QWidget *parent = nullptr);
    ~PxiCycloMainWindow();
    void selectPage(GlobalDefinitions::DataFormPageType);

  signals:
    void selectedDataPageSignal(GlobalDefinitions::DataFormPageType);

  protected:
    virtual void closeEvent(QCloseEvent* event);

private:
    Ui::PxiCMainWindow *ui;

    QToolBar* m_pToolBar;

    QTabWidget* m_pDataFormPageTabWidget;

    ThermoDataForm* m_pThermoDataForm;
    PxiDevicesDataForm* m_pPxiDevicesDataForm;

    QIcon* m_pLoadDataIcon;
    QIcon* m_pSaveDataIcon;
    QIcon* m_pClearDataIcon;

    QIcon* m_pDevicesCyclogram;
    QIcon* m_pThermoCyclogram;

    // methods
    void setupUI();
    void setupToolBar();

    GlobalDefinitions::DataFormPageType m_nSelectedDataPage;

  private slots:
    void selectedPageChangedSlot(int);

};
#endif // MAINWINDOW_H

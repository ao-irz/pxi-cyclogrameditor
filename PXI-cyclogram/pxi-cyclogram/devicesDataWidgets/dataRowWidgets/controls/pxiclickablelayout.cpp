#include "pxiclickablelayout.h"

PxiClickableLayout::PxiClickableLayout(const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget* parent) : QHBoxLayout()
  ,m_bSelected(false)
{

    m_pL1 = new PxiClickableLabel("", parent);
    m_pL1->setIcons(normalIcon,selectedIcon);

    m_pL2 = new PxiClickableLabel(strLabelText, parent);

    connect(m_pL1,&PxiClickableLabel::clicked,this,&PxiClickableLayout::iconLabelClicked);
    connect(m_pL2,&PxiClickableLabel::clicked,this,&PxiClickableLayout::textLabelClicked);

    setSpacing(4);
    addWidget(m_pL1);
    addWidget(m_pL2,Qt::AlignLeft);

    unselect();
}

void PxiClickableLayout::select()
{
    m_pL1->select();
    m_pL2->select();

    m_bSelected = true;
}

void PxiClickableLayout::unselect()
{
    m_pL1->unselect();
    m_pL2->unselect();

    m_bSelected = false;
}

void PxiClickableLayout::iconLabelClicked()
{
    if ( !m_bSelected )
    {
        m_bSelected = true;

        m_pL2->select();
        emit clicked();
    }
}

void PxiClickableLayout::textLabelClicked()
{
    if ( !m_bSelected )
    {
        m_bSelected = true;

        m_pL1->select();
        emit clicked();
    }
}

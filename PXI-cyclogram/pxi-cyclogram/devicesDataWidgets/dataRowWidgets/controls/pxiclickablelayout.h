#ifndef PXICLICKABLELAYOUT_H
#define PXICLICKABLELAYOUT_H

#include <QObject>
#include <QHBoxLayout>

#include "global_definitions.h"
#include "../../../common_controls/pxiclickablelabel.h"

class PxiClickableLayout : public QHBoxLayout
{
    Q_OBJECT

public:
    explicit PxiClickableLayout(const QString strLabelText,
                                const QPixmap& normalIcon,
                                const QPixmap& selectedIcon,
                                QWidget* parent);
    void select();
    void unselect();

signals:
    void clicked();

private:
    PxiClickableLabel* m_pL1;
    PxiClickableLabel* m_pL2;
    bool m_bSelected;

private slots:
    void iconLabelClicked();
    void textLabelClicked();
};

#endif // PXICLICKABLELAYOUT_H

#ifndef PXIDEVICEDATAROWSMU_H
#define PXIDEVICEDATAROWSMU_H

#include <QObject>

#include "pxidevicedatarow.h"

class PxiDeviceDataRowSmu : public PxiDeviceDataRow
{
    Q_OBJECT

public:
    PxiDeviceDataRowSmu(const int nDeviceIndex,
                       const QString strLabelText,
                       const QPixmap& normalIcon,
                       const QPixmap& selectedIcon,
                       QWidget *parent = nullptr);

    double voltageValue() const;
    double voltageTolerancePlusValue() const;
    double voltageToleranceMinusValue() const;

public slots:
    virtual void clearDetailsWidget() override;

protected:
    virtual void setupDetailsArea() override;
    virtual void updateDetailsArea() override;
    virtual void storeDataControlContents() override;
    virtual void fillDataControlContents() override;

protected slots:
    virtual void switchStateChangedSlot(bool) override;

private:
    QCheckBox* m_pUseVoltageCheck;
    QDoubleSpinBox* m_pVoltageEdit;
    QDoubleSpinBox* m_pVoltageTolerancePlusEdit;
    QDoubleSpinBox* m_pVoltageToleranceMinusEdit;

    QDoubleSpinBox* m_pCurrentEdit;
    QDoubleSpinBox* m_pCurrentTolerancePlusEdit;

    QSpinBox* m_pPinNumber;

  private slots:
    void useVoltageCheckStateChangedSlot(int);

};

#endif // PXIDEVICEDATAROWSMU_H

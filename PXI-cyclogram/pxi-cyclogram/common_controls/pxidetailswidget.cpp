#include "pxidetailswidget.h"
#include "../global_definitions.h"

QPixmap* PxiDetailsWidget::m_pAddBeforePict = nullptr;
QPixmap* PxiDetailsWidget::m_pAddAfterPict = nullptr;
QPixmap* PxiDetailsWidget::m_pMinusPict = nullptr;
QPixmap* PxiDetailsWidget::m_pClearPict = nullptr;

PxiDetailsWidget::PxiDetailsWidget(const QString& strDeviceName,
                                   QWidget *parentWidget, bool withClearButton) : QWidget(parentWidget)
  ,m_strDeviceName(strDeviceName)
  ,m_bWithClearButton(withClearButton)
{
    m_pMainLayout = new QHBoxLayout();

    m_pDetailsLayout = new QGridLayout();
    m_pDeviceSwitcher = new PxiSwitcher(this);
    m_pDetailsLayout->addWidget(m_pDeviceSwitcher,0,0,2,1,Qt::AlignTop);
    m_pMainLayout->addLayout(m_pDetailsLayout,1);

    m_pButtonLayout = new QVBoxLayout();
    m_pButtonLayout->setSpacing(2);

    if ( !m_pAddBeforePict )
      m_pAddBeforePict = new QPixmap(ADD_BEFORE_ICON);
    if ( !m_pAddAfterPict )
      m_pAddAfterPict = new QPixmap(ADD_AFTER_ICON);
    if ( !m_pMinusPict )
      m_pMinusPict = new QPixmap(MINUS_ICON);
    if ( !m_pClearPict )
      m_pClearPict = new QPixmap(CLEAR_ICON);

    m_pAddBlockBeforeButton = new QPushButton(*(new QIcon(*m_pAddBeforePict)),"",this);
    m_pAddBlockBeforeButton->setToolTip(ADD_BLOCK_BEFORE_TOOLTIP);
    m_pAddBlockBeforeButton->setIconSize(QSize(50,12));
    m_pAddBlockAfterButton = new QPushButton(*(new QIcon(*m_pAddAfterPict)),"",this);
    m_pAddBlockAfterButton->setToolTip(ADD_BLOCK_AFTER_TOOLTIP);
    m_pAddBlockAfterButton->setIconSize(QSize(50,12));
    m_pDeleteBlockButton = new QPushButton(*(new QIcon(*m_pMinusPict)),"",this);
    m_pDeleteBlockButton->setToolTip(DELETE_BLOCK_TOOLTIP);
    m_pClearBlockButton = new QPushButton(*(new QIcon(*m_pClearPict)),"",this);
    m_pClearBlockButton->setToolTip(CLEAR_BLOCK_TOOLTIP);
    m_pButtonLayout->addWidget(m_pAddBlockBeforeButton);
    m_pButtonLayout->addWidget(m_pAddBlockAfterButton);
    m_pButtonLayout->addWidget(m_pDeleteBlockButton);
    m_pButtonLayout->addWidget(m_pClearBlockButton,1,Qt::AlignTop);
    if ( !m_bWithClearButton )
    {
      m_pClearBlockButton->setEnabled(false);
    }

    m_pMainLayout->addLayout(m_pButtonLayout,0);

    setLayout(m_pMainLayout);

    connect(m_pAddBlockBeforeButton,&QPushButton::clicked,
            this,&PxiDetailsWidget::addNewDataBlockBeforeSignal);
    connect(m_pDeleteBlockButton,&QPushButton::clicked,
            this,&PxiDetailsWidget::deleteDataBlockSignal);
    connect(m_pAddBlockAfterButton,&QPushButton::clicked,
            this,&PxiDetailsWidget::addNewDataBlockAfterSignal);
    connect(m_pClearBlockButton,&QPushButton::clicked,
              this,&PxiDetailsWidget::clearCurrentBlockSignal);
}

QGridLayout *PxiDetailsWidget::detailsLayout() const
{
    return m_pDetailsLayout;
}

PxiSwitcher* PxiDetailsWidget::switcher() const
{
    return m_pDeviceSwitcher;
}


QFrame* PxiDetailsWidget::separatorFrame()
{
  QFrame* vSeparator = new QFrame(static_cast<QWidget*>(this));
  vSeparator->setFrameShape(QFrame::VLine);
  vSeparator->setFrameShadow(QFrame::Sunken);
  vSeparator->setContentsMargins(0,0,0,0);

  return vSeparator;
}

QWidget* PxiDetailsWidget::createLabeledFieldWidget(QString strLabel,
                                       QWidget* pWidget,
                                       bool addStretch)
{
  QWidget* theWidget = new QWidget(pWidget->parentWidget());

  QVBoxLayout* layout = new QVBoxLayout();
  layout->setMargin(0);
  layout->setSpacing(2);

  theWidget->setLayout(layout);

  QLabel* l = new QLabel(strLabel,theWidget);
  l->setContentsMargins(0,0,0,0);
  layout->addWidget(l);

  pWidget->setParent(theWidget);
  if ( addStretch )
  {
    QHBoxLayout* bottomHLayout = new QHBoxLayout();
    bottomHLayout->setMargin(0);
    bottomHLayout->setSpacing(0);
    bottomHLayout->addWidget(pWidget,0,Qt::AlignLeft);
    bottomHLayout->addStretch(1);
    layout->addLayout(bottomHLayout);
  }
  else
    layout->addWidget(pWidget);

  return theWidget;
}

QVBoxLayout* PxiDetailsWidget::createLabeledFieldVLayout(QString strLabel,
                                                         QWidget* pWidget,
                                                         bool addHStretch,
                                                         bool addVStretch)
{
    QVBoxLayout* layout = new QVBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(2);

    QLabel* l = new QLabel(strLabel,pWidget->parentWidget());
    l->setContentsMargins(0,0,0,0);
    layout->addWidget(l);

    if ( addHStretch )
    {
      QHBoxLayout* bottomHLayout = new QHBoxLayout();
      bottomHLayout->setMargin(0);
      bottomHLayout->setSpacing(0);
      bottomHLayout->addWidget(pWidget,0,Qt::AlignLeft);
      bottomHLayout->addStretch(1);
      layout->addLayout(bottomHLayout);
    }
    else
      layout->addWidget(pWidget);

    if ( addVStretch )
      layout->addStretch(1);

    return layout;
}

QVBoxLayout* PxiDetailsWidget::createCheckableFieldVLayout(QString strLabel,
                                         QWidget* pWidget,
                                         QCheckBox*& pCheckBox,bool addStretch )
{
  QVBoxLayout* layout = new QVBoxLayout();
  layout->setMargin(0);
  layout->setSpacing(2);

  QHBoxLayout* topLayout = new QHBoxLayout();
  topLayout->setSpacing(0);
  topLayout->setContentsMargins(0,0,0,0);
  QLabel* l = new QLabel(strLabel,pWidget->parentWidget());
  l->setContentsMargins(0,0,4,2);
  QCheckBox* checkBox = new QCheckBox(pWidget->parentWidget());
  checkBox->setContentsMargins(0,0,0,0);
  topLayout->addWidget(l,0,Qt::AlignLeft);
  topLayout->addWidget(checkBox,0,Qt::AlignLeft);
  topLayout->addStretch(1);
  layout->addLayout(topLayout);

  if ( addStretch )
  {
    QHBoxLayout* bottomHLayout = new QHBoxLayout();
    bottomHLayout->setMargin(0);
    bottomHLayout->setSpacing(0);
    bottomHLayout->addWidget(pWidget,0,Qt::AlignLeft);
    bottomHLayout->addStretch(1);
    layout->addLayout(bottomHLayout);
  }
  else
    layout->addWidget(pWidget);

  pWidget->setEnabled(false);

  connect(checkBox,&QCheckBox::stateChanged,
          [pWidget](int checkState){pWidget->setEnabled(checkState==Qt::Checked);});

  pCheckBox = checkBox;

  return layout;
}

void PxiDetailsWidget::dataRowBlocksCountChangedSlot(int blocksCount)
{
  m_pAddBlockBeforeButton->setEnabled(blocksCount>0);
  m_pClearBlockButton->setEnabled(blocksCount>0 && m_bWithClearButton);
  m_pDeleteBlockButton->setEnabled(blocksCount>0);
  m_pDeviceSwitcher->setEnabled(blocksCount>0);
}

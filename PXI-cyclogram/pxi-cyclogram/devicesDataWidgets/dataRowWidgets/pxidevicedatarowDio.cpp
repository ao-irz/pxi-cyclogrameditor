#include "pxidevicedatarowDio.h"
#include "../../global_definitions.h"
#include <QJsonArray>

PxiDeviceDataRowDio::PxiDeviceDataRowDio(const int nDeviceIndex,
                                       const QString strLabelText,
                                       const QPixmap& normalIcon,
                                       const QPixmap& selectedIcon,
                                       QWidget *parent):
    PxiDeviceDataRow(GlobalDefinitions::ItemType::ITEM_DIO,
                     nDeviceIndex,
                     strLabelText,
                     normalIcon,
                     selectedIcon,
                     parent)
{
    setupDurationArea();
    setupDetailsArea();
}

double PxiDeviceDataRowDio::voltageLogic() const
{
  return m_pVoltageLogicBox->currentText().toDouble();
}

void PxiDeviceDataRowDio::clearDetailsWidget()
{
  m_bNoSave = true;

  PxiDeviceDataRow::clearDetailsWidget();

  for( auto pPinWidget: m_lstPins )
    pPinWidget->setChecked(false);

  m_pVoltageLogicBox->setCurrentIndex(0);

  m_bNoSave = false;
}

void PxiDeviceDataRowDio::setupDetailsArea()
{
  // там выставляются контролы времени, единые для всех
  PxiDeviceDataRow::setupDetailsArea();

  QGridLayout* pLayout = m_pDetailsWidget->detailsLayout();

  m_pVoltageLogicBox = new QComboBox(m_pDetailsWidget);
  m_pVoltageLogicBox->setProperty( JSON_FIELD_NAME ,JSON_DIO_VOLTAGE_LOGIC);
  m_pVoltageLogicBox->addItems({"1.8","2.5","3.3","5.0"});
  pLayout->addLayout(m_pDetailsWidget->createLabeledFieldVLayout("Напр. логики",m_pVoltageLogicBox),
                     0,m_setupControlColumn);
  connect(m_pVoltageLogicBox,&QComboBox::currentTextChanged,
          this,&PxiDeviceDataRowDio::dataControlValueChangedSlot);

  for(int i=0; i<8; i++)
  {
    PxiDioPin* pPinWidget = new PxiDioPin(i,m_pDetailsWidget);
    pPinWidget->setProperty( JSON_FIELD_NAME ,JSON_DIO_PIN);
    m_lstPins.append(pPinWidget);
    if ( i<4 )
      pLayout->addWidget(pPinWidget,1,m_setupControlColumn+i);
    else
      pLayout->addWidget(pPinWidget,2,m_setupControlColumn+i-4);

    connect(pPinWidget,&PxiDioPin::valueChanged,
            this,&PxiDeviceDataRowDio::dataControlValueChangedSlot);
  }
}

void PxiDeviceDataRowDio::updateDetailsArea()
{
  PxiDeviceDataRow::updateDetailsArea();

  if ( m_pActiveSection )
  {
    m_pVoltageLogicBox->setEnabled(true);

    for( auto pPinWidget: m_lstPins )
      pPinWidget->setEnabled(true);

    fillDataControlContents();
  }
  else
  {
    m_pVoltageLogicBox->setEnabled(false);

    for( auto pPinWidget: m_lstPins )
    {
      pPinWidget->setChecked(false);
      pPinWidget->setEnabled(false);
    }
  }
}

void PxiDeviceDataRowDio::storePinsArray()
{
  m_pActiveSection->dataBlock()->remove(JSON_DIO_PINS);
  QJsonArray pinsArray;
  for( auto pPinWidget: m_lstPins )
  {
    if ( pPinWidget->isChecked())
    {
      QJsonObject pinObject;
      pinObject.insert(JSON_DIO_PIN_NUMBER,QJsonValue(pPinWidget->pinNumber()));
      pinObject.insert(JSON_DIO_PIN_MODE,QJsonValue(pPinWidget->pinMode()));
      pinObject.insert(JSON_DIO_PIN_VALUE,QJsonValue(pPinWidget->pinValue()));
      pinsArray.append(pinObject);
    }
  }
  if ( pinsArray.count()>0 )
    m_pActiveSection->dataBlock()->insert(JSON_DIO_PINS,pinsArray);
}

void PxiDeviceDataRowDio::storeDataControlContents()
{
  if ( !m_pActiveSection )
    return;

  PxiDeviceDataRow::storeDataControlContents();

  m_pActiveSection->dataBlock()->insert(JSON_DIO_VOLTAGE_LOGIC,
                        QJsonValue(voltageLogic()));
  storePinsArray();
}

void PxiDeviceDataRowDio::fillDataControlContents()
{
  m_bNoSave = true;

  PxiDeviceDataRow::fillDataControlContents();

  m_pStartTimeCheckBox->setEnabled(false);

  if ( m_pActiveSection )
  {
    m_pVoltageLogicBox->setCurrentText(QString::number(m_pActiveSection->dataBlock()->value(JSON_DIO_VOLTAGE_LOGIC).toDouble()));

    for( auto pPinWidget: m_lstPins )
      pPinWidget->setChecked(false);

    if ( !(m_pActiveSection->dataBlock()->value(JSON_DIO_PINS) == QJsonValue::Undefined) )
    {
      QJsonArray pinsArray = m_pActiveSection->dataBlock()->value(JSON_DIO_PINS).toArray();
      for( auto pinsArrayElement : pinsArray )
      {
        QJsonObject psObject = pinsArrayElement.toObject();
        m_lstPins[psObject.value(JSON_DIO_PIN_NUMBER).toInt()]->setChecked(true);
        m_lstPins[psObject.value(JSON_DIO_PIN_NUMBER).toInt()]->setMode(psObject.value(JSON_DIO_PIN_MODE).toString());
        m_lstPins[psObject.value(JSON_DIO_PIN_NUMBER).toInt()]->setValue(psObject.value(JSON_DIO_PIN_VALUE).toBool());
      }
    }
  }

  m_bNoSave = false;
}

void PxiDeviceDataRowDio::switchStateChangedSlot(bool bIsNewStateOn)
{
  if ( !m_pActiveSection )
    return;

  if ( !bIsNewStateOn || !checkForSimultaneousTurnOn(GlobalDefinitions::ItemType::ITEM_SMU) )
    PxiDeviceDataRow::switchStateChangedSlot( bIsNewStateOn );
}

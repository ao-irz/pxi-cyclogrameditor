QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

TARGET = PXI_cyclogram_editor

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    common_controls/pxidurationcontrolswidget.cpp \
    common_controls/pxiswitcher.cpp \
    common_controls/pxicheckabletoolbutton.cpp \
    common_controls/pxiclickablelabel.cpp \
    common_controls/pxidetailswidget.cpp \
  dataModel/scheduledatablock.cpp \
    devicesDataWidgets/dataRowWidgets/controls/pxidiopin.cpp \
    devicesDataWidgets/dataRowWidgets/controls/pxidurationbar.cpp \
    devicesDataWidgets/dataRowWidgets/controls/pxiclickablelayout.cpp \
  devicesDataWidgets/dataRowWidgets/controls/thermodurationbar.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarow.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowDio.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowGen.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowPS.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowSmu.cpp \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowTC.cpp \
   	devicesDataWidgets/dataRowWidgets/pxischedulesection.cpp \
  devicesDataWidgets/dataRowWidgets/thermodurationwidget.cpp \
    devicesDataWidgets/dataRowWidgets/thermoschedulesection.cpp \
  devicesDataWidgets/dataform.cpp \
  devicesDataWidgets/thermodataform.cpp \
    mainwindow.cpp \
    main.cpp \
		devicesDataWidgets/pxidevicesdataform.cpp

HEADERS += \
    common_controls/pxidurationcontrolswidget.h \
    common_controls/pxiswitcher.h \
    common_controls/pxicheckabletoolbutton.h \
    common_controls/pxiclickablelabel.h \
    common_controls/pxidetailswidget.h \
    dataModel/scheduledatablock.h \
    devicesDataWidgets/dataRowWidgets/controls/pxidiopin.h \
    devicesDataWidgets/dataRowWidgets/controls/pxidurationbar.h \
    devicesDataWidgets/dataRowWidgets/controls/pxiclickablelayout.h \
    devicesDataWidgets/dataRowWidgets/controls/thermodurationbar.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarow.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowDio.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowGen.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowPS.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowSmu.h \
    devicesDataWidgets/dataRowWidgets/pxidevicedatarowTC.h \
    devicesDataWidgets/dataRowWidgets/pxischedulesection.h \
    devicesDataWidgets/dataRowWidgets/thermodurationwidget.h \
    devicesDataWidgets/dataRowWidgets/thermoschedulesection.h \
    devicesDataWidgets/dataform.h \
    devicesDataWidgets/thermodataform.h \
    global_definitions.h \
    mainwindow.h \
    devicesDataWidgets/pxidevicesdataform.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    PXI_cyclo_res.qrc

